<?php

use yii\bootstrap\Modal;

?>
<!--add spacing-->
<hr class="space">

<!-- 950px width centered-->
<div class="container">
    <div class="span-8">
        <div class="boxed_script">
            <h3>Свежий взгляд на подбор персонала</h3>
            <p style="text-align: justify;">Онлайн-сервис Lime-prime позволяет провести дистанционное интервью с кандидатами. В результате у Вас есть возможность посмотреть видеозапись,&nbsp;на которой кандидат отвечает&nbsp;на Ваши вопросы.</p>
            <p><a id="signup" title="Регистрация" href="javascript:;">
                    <img src="assets\images\Reg_opt1.png" alt="" width="270" height="113"></a></p>
        </div>
        <div class="box_bg_2"></div>
    </div>
    <div class="span-16">
        <link rel="stylesheet" type="text/css" href="assets\green\css\siteFeature.style_screen.css">
        <script src="assets\green\js\jquery.siteFeature.js" type="text/javascript"></script>
        <script type="text/javascript">
        // <![CDATA[
        $(document).ready(function() {
            $('#preFeature').siteFeature({

                txtBoxAnimateInHorzType: 'slideDown',
                txtBoxAnimateOutHorzType: 'slideDown',
                txtBoxPauseBetweenInOut: 1000,
                autoPlay: true,
                autoPlayInterval: 5000
            });
        });
        // ]]>
        </script>
        <div id="DemoContainer">
            <div id="preFeature">
                <div>
                    <p><img src="assets\green\images\image01.jpeg" alt="alt text"></p>
                    <h3>Вы формулируете вопросы</h3>
                    <p>ответы на которые хотели бы услышать от кандидата, а так же его e-mail.</p>
                    <!--<a href="http://www.google.com">подробнее</a>-->
                </div>
                <div>
                    <p><img src="assets\green\images\image02.jpeg" alt="alt text"></p>
                    <h3>Кандидат получает приглашение</h3>
                    <p>по электронной почте со ссылкой на страницу, на которой последовательно отображаются Ваши вопросы.</p>
                    <!--<a href="ссылка на страницу">подробнее</a>-->
                </div>
                <div>
                    <p><img src="assets\green\images\image03.jpeg" alt="alt text"></p>
                    <h3>Кандидат отвечает на вопросы</h3>
                    <p>в удобное для него время, при этом ведется видеозапись при помощи веб-камеры.</p>
                    <!--<a href="ссылка">подробнее</a>-->
                </div>
                <div>
                    <p><img src="assets\green\images\image04.jpeg" alt="alt text"></p>
                    <h3>Вы смотрите видеозапись</h3>
                    <p>получив по почте уведомление о записи. В личном кабинете можете посмотреть уже записанные интервью.</p>
                    <!--<a href="ссылка">подробнее</a>-->
                </div>
            </div>
        </div>
        <div class="box_bg_630"></div>
    </div>
    <hr class="space">
    <script type="text/javascript">
    $(document).ready(function() {
        $(".servis_m").colorbox({
            width: 650,
            inline: true,
            href: "#box_serv"
        });
    });
    </script>
    <div class="span-8">
        <div class="boxed_small">
            <h3>Подробнее о сервисе</h3>
            <p><a class="servis_m" title="" href="#">
                    <img class="right" src="assets\green\images\front\IM_opt.png" alt=""></a>
                <p>&nbsp;</p>
                <p>Как это работает?</p>
                <p>В чем преимущества?</p>
                <p>На кого ориентирован сервис?</p>
                <p>&nbsp;</p>
                <p><strong><a class="servis_m" href="index-2.html?id=49">читать &gt;&gt;</a></strong></p>
                <p><strong><br></strong></p>
            </p>
        </div>
        <div class="box_bg_2"></div>
    </div>
    <div style='display:none'>
        <div id="box_serv">
            <div class="span-15">
                <div class="footbox">
                    <h2>Подробнее о сервисе</h2>
                    <hr>
                    <h3 style="font-size: 1.17em;">Как это работает</h3>
                    <ul>
                        <li>Вы формулируете и вводите вопросы, ответы на которые хотели&nbsp;бы услышать от кандидата, а так же его e-mail.</li>
                        <li>Кандидат получает по электронной почте приглашение со&nbsp;ссылкой на страницу, на которой последовательно отображаются&nbsp;Ваши вопросы.</li>
                        <li>Кандидат в удобное для него время отвечает на предложенные&nbsp;вопросы, при этом ведется видеозапись при помощи веб-камеры.</li>
                        <li>Вы получаете по почте уведомление о записи и ссылку на&nbsp;страницу с видеозаписью.</li>
                    </ul>
                    <p>&nbsp;</p>
                    <h3 style="font-size: 1.17em; ">Преимущества</h3>
                    <p><strong>Экономия денег</strong></p>
                    <p>Очное собеседование, как правило, является наиболее&nbsp;затратным и по деньгам, и по времени. Особенно актуально это при&nbsp;подборе кандидатов в регионах, так как в стоимость проведения&nbsp;собеседования входят как минимум билеты и проживание.</p>
                    <p>Lime-prime минимизирует расходы и позволяет повысить процент&nbsp;результативности очных встреч за счет предварительного отбора&nbsp;кандидатов.</p>
                    <p><strong>Экономия времени</strong></p>
                    <p>Сколько кандидатов вы можете оценить в день при очном&nbsp;собеседовании? Три? Пять? А если они все в разных городах или&nbsp;даже странах?</p>
                    <p>Основываясь на опыте реальных проектов при использовании Limeprime,&nbsp;мы можем с уверенностью говорить о возможности оценки&nbsp;15-20 кандидатов в день.</p>
                    <p><strong>Удобство</strong></p>
                    <p>Запись в любое время, в любом месте&nbsp;Ни Вы, ни Кандидат не привязаны к конкретному времени и месту&nbsp;проведения интервью. Запись и просмотр возможны и в обеденный перерыв в интернет-кафе и вечером в домашних условиях.</p>
                    <p><strong>Коллективное обсуждение и принятие решений</strong></p>
                    <p>В оценке каждого кандидата и принятии решения могут&nbsp;участвовать и Ваши коллеги. Каждый имеющий доступ к базе&nbsp;имеет возможность выставлять оценки и оставлять комментарии.</p>
                    <p><strong>Доказательная база принятого решения</strong></p>
                    <p>У Вас всегда есть возможность обосновать свое решение по&nbsp;кандидату, предоставив видеозапись интервью со своими&nbsp;комментариями и оценкой каждого фрагмента.</p>
                    <p>&nbsp;</p>
                    <p>&nbsp;</p>
                    <hr class="space">
                    <div class="hrbg2">hrbg</div>
                    <!--port entry close-->
                </div>
            </div>
        </div>
    </div>
    <div class="span-8">
        <div class="boxed_small">
            <h3>Экскурсия по Lime-Prime</h3>
            <p><a class="prezent" title="" href="index-3.html?id=48">
                    <img class="right" src="assets\green\images\front\Television-Alt_opt.png" alt=""></a>
                <p>Посмотрите видео,</p>
                <p>
                    на котором</p>
                <p>
                    показан&nbsp;алгоритм
                </p>
                <p>использования
                </p>
                <p>Lime-Prime.</p>
                <p>&nbsp;</p>
                <p><strong><a class="prezent" href="index-3.html?id=48">смотреть &gt;&gt;</a></strong></p>
            </p>
        </div>
        <div class="box_bg_2"></div>
    </div>
    <script type="text/javascript">
    $(document).ready(function() {
        $(".iframe").colorbox({
            width: 870,
            height: 330,
            iframe: true,
            onComplete: function() {
                $("#cboxLoadedContent").css("margin-top", "15px");
            }
        });

    });
    </script>
    <div class="span-8 last">
        <div class="boxed_small">
            <h3>Интервью с Lime-Prime</h3>
            <p><a class="iframe" href="index-4.html?id=113&amp;lcid=1">
                    <img class="right" src="assets\green\images\front\Contact_opt.png" alt=""></a>
                <p>&nbsp;</p>
                <p>В качестве примера - видео-интервью c сотрудником </p>
                <p>Lime-Prime.</p>
                <p>&nbsp;</p>
                <p><strong><a class="iframe" href="index-4.html?id=113&lcid=1">смотреть &gt;&gt;</a></strong></p>
            </p>
        </div>
        <div class="box_bg_2"></div>
    </div>

    <hr class="space">
    <!--add image for hr -->
    <div class="hrbg">hrbg</div>
    <!--Declare 630px width-->
    <div class="span-16">
        <div class="footer_text">
            <p> &nbsp;
                <a href="index-5.html?id=52" class="fot1">Условия использования</a> &nbsp;&nbsp; | &nbsp;&nbsp;
                <a href="index-6.html?id=53" class="fot2">Политика конфиденциальности</a> &nbsp;&nbsp; | &nbsp;&nbsp;
                <a href="index-7.html?id=54" class="fot3">Требования к ПО</a> &nbsp;&nbsp; | &nbsp;&nbsp;
                <a href="#" class="fot4">Пресс-релиз</a> &nbsp;&nbsp; | &nbsp;&nbsp;
                <a href="index-8.html?id=131" class="fot5">Контакты</a>.
            </p>
        </div>
        <!--footer_text close-->

    </div>
    <!--footer close-->

    <!--Footer navigation goes here-->
    <!--Declare 310px width-->
    <div class="span-8 last">
        <!--footer-nav close-->
        <ul id="footer-nav">
            <li><a href="index.html">Главная</a></li>
            <li>| <a href="features.html" title="Как пользоваться сервисом?">Как пользоваться?</a></li>
            <!--<li>| <a href="#" title="Как пользоваться сервисом?">Как пользоваться?</a></li>-->
            <li>| <a href="conditions.html" title="Условия использования сервиса Lime Prime">Условия</a></li>
            <!--<li>| <a href="#" title="Условия использования сервиса Lime Prime">Условия</a></li>-->

            <li>| <a href="index-1.html?id=51" title="" id="login1" class="login">Вход</a></li>
        </ul>
        <!--This is an IE6 workaround for problems rendering jquery sliding text-overs.-->
    </div>
    <!--footer nav close-->

</div>
<!--span-24 close-->

<!--container close-->
<div style="display:none">
    <div id="fot4" style="padding:0px; background:#fff;">
        <div class="span-15">
            <div class="footbox">
                <h2>Lime-Prime - сервис веб-интервью кандидатов на должность в Вашей компании</h2>
                <p><img src="assets\images\logo.jpeg" alt="Логотип Lime-Prime" width="258" height="63"></p>
                <h2>Основная идея</h2>
                <p>Онлайн-сервис Lime-prime позволяет провести дистанционное&nbsp;интервью с соискателями. В результате у Вас есть возможность&nbsp;посмотреть видеозапись, на которой кандидат отвечает на Ваши&nbsp;вопросы.</p>
                <p>&nbsp;</p>
                <h2>Как это работает</h2>
                <ul>
                    <li>Вы формулируете и вводите вопросы, ответы на которые хотели&nbsp;бы услышать от кандидата, а так же его e-mail.</li>
                    <li>Кандидат получает по электронной почте приглашение со&nbsp;ссылкой на страницу, на которой последовательно отображаются&nbsp;Ваши вопросы.</li>
                    <li>Кандидат в удобное для него время отвечает на предложенные&nbsp;вопросы, при этом ведется видеозапись при помощи веб-камеры.</li>
                    <li>Вы получаете по почте уведомление о записи и ссылку на&nbsp;страницу с видеозаписью.</li>
                </ul>
                <div>
                    <h2>Преимущества</h2>
                    <h3>Экономия денег</h3>
                    <p>Стандартная процедура подбора состоит из следующих пунктов:&nbsp;</p>
                    <ul>
                        <li>Размещение вакансии на специализированных сайтах либо&nbsp;заявка в кадровые агентства.</li>
                        <li>Анализ резюме.</li>
                        <li>Телефонное собеседование.</li>
                        <li>Очное собеседование.</li>
                    </ul>
                    <p>Именно очное собеседование, как правило, является наиболее&nbsp;затратным и по деньгам, и по времени. Особенно актуально это при&nbsp;подборе кандидатов в регионах, так как в стоимость проведения&nbsp;собеседования входят как минимум билеты и проживание.</p>
                    <p>Lime-prime минимизирует расходы и позволяет повысить процент&nbsp;результативности очных встреч за счет предварительного отбора&nbsp;кандидатов.</p>
                    <p>&nbsp;</p>
                    <h3>Экономия времени</h3>
                    <p>Сколько кандидатов вы можете оценить в день при очном&nbsp;собеседовании? Три? Пять? А если они все в разных городах или&nbsp;даже странах?</p>
                    <p>Основываясь на опыте реальных проектов при использовании Lime-Prime,&nbsp;мы можем с уверенностью говорить о возможности оценки&nbsp;15-20 кандидатов в день (одним специалистом).</p>
                    <p>&nbsp;</p>
                    <h3>Удобство</h3>
                    <p>Запись в любое время, в любом месте&nbsp;Ни Вы, ни Кандидат не привязаны к конкретному времени и месту&nbsp;проведения интервью. Запись и просмотр возможны и в обеденный&nbsp;перерыв в интернет-кафе и вечером в домашних условиях.</p>
                    <p>&nbsp;</p>
                    <h3>Коллективное обсуждение и принятие решений</h3>
                    <div>
                        <p>В оценке каждого кандидата и принятии решения могут&nbsp;участвовать и Ваши коллеги. Каждый имеющий доступ к базе&nbsp;имеет возможность выставлять оценки и оставлять комментарии.</p>
                        <p>&nbsp;</p>
                        <p><span style="font-size: 12px; font-weight: bold;">Доказательная база принятого решения</span></p>
                        <div>
                            <p>У Вас всегда есть возможность обосновать свое решение по&nbsp;кандидату, предоставив видеозапись интервью со своими&nbsp;комментариями и оценкой каждого фрагмента.</p>
                            <p>&nbsp;</p>
                            <h2>На кого ориентирован сервис</h2>
                            <h3>Корпоративные службы персонала, руководители</h3>
                            <p>&nbsp;</p>
                            <p>Расширяете бизнес? Хотите открыть филиал или найти партнеров?&nbsp;Ищете сотрудников в регионах или за рубежом? lime-prime - это то,&nbsp;что Вам нужно! С помощью сервиса так же возможно проведение&nbsp;дистанционной аттестации.</p>
                            <p>&nbsp;</p>
                            <h3>Кадровые агентства</h3>
                            <p>Возможность повысить привлекательность, ценность и&nbsp;технологичность предоставляемых услуг без значительных&nbsp;финансовых вложений!&nbsp;</p>
                            <p>На Ваш сайт устанавливается виджет, который позволяет&nbsp;записывать видео-резюме. Внешний вид виджета можно&nbsp;адаптировать под стиль Вашего сайта.</p>
                            <p>По желанию Ваших клиентов Вы можете помимо стандартных&nbsp;резюме предоставлять видео-резюме.</p>
                            <p>&nbsp;</p>
                            <p>&nbsp;</p>
                            <p>&nbsp;</p>
                            <p>&nbsp;</p>
                        </div>
                        <p>&nbsp;</p>
                    </div>
                </div>
                <!--port entry close-->
            </div>
        </div>
    </div>
</div>
<!--IE Fix for over-shadow text replacement-->
<script type="text/javascript">
Cufon.now();
</script>
<script type="text/javascript">
    $(document).ready(function(){   
        $("#signup").click(function(){
            $.ajax({
                type: "GET",
                url: "frontend/web/site/signup",
                data: {'<?= Yii::$app->request->csrfParam ?>': '<?= \yii::$app->request->csrfToken ?>'}, 
                success: function(data) {
                    $("body").append(data);
                },  
                error: function(xhr, str) {
                
                }
            });
        });
    });
</script>
<!-- lime beget-->

<!-- BEGIN JIVOSITE CODE {literal} -->
<!--<script type='text/javascript'>
(function(){ var widget_id = 'Wj1RtCzdWc';var d=document;var w=window;function l(){
var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();</script>-->
<!-- {/literal} END JIVOSITE CODE -->