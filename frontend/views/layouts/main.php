<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use yii\bootstrap\ActiveForm;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
    <!--<meta name="viewport" content="width=device-width, initial-scale=1">-->
    <?php $this->registerCsrfMetaTags() ?>

    <!----------------------!!!!!!!!!!!------------------------------------------------------------------------------->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Lime-Prime. Сервис дистанционного подбора, оценки, аттестации персонала с помощью видео-интервью.</title>
    <base href="http://lime-prime.local">
    <meta name="verify-reformal" content="7fe22c9dfe704ced5e59c13d">
    <!-- CSS Links-->
    <!--To make sure this template was viewed correctly in the majority of browsers, there are several css files.-->

    <!--6 CSS files total, listed below-->

    <!--Blueprint Framework CSS-->
    <!--2 CSS IE Render Fixes-->
    <!--Custom Styling (site_styles.css)-->
    <!--Colorbox Styling-->
    <!--jQuery Slider CSS-->
    <link rel="icon" type="image/png" href="favicon.png">
    <!-- To allow for easy manipulation, I have -->
    <!--left the Blueprint Framework untouched, simply overwriting styles in the site_style.css. To make any -->
    <!--changes to the template, simply change/modify styles in the site_styles.css file. -->
    <link rel="stylesheet" type="text/css" href="assets\jsmod-l\resources\css\ext-all.css">
    <link rel="stylesheet" type="text/css" href="assets\jsmod-l\style.css">
    <link rel="stylesheet" href="assets\green\css\framework.css" type="text/css" media="screen, projection">
    <link rel="stylesheet" href="assets\green\css\site_styles.css" type="text/css" media="screen, projection">
    <link rel="stylesheet" type="text/css" href="assets\green\css\EA_Form.css">
    <!--IE 6,7 Render Fixes-->
    <!--[if lt IE 8]><link rel="stylesheet" href="assets/green/css/ie.css" type="text/css" media="screen, projection"><![endif]-->
    <!--[if lt IE 7]><link rel="stylesheet" href="assets/green/css/ie6.css" type="text/css" media="screen, projection"><![endif]-->

    <!--Colorbox image stylings-->
    <link rel="stylesheet" href="assets\plugins\qm\css\colorbox.css" type="text/css" media="screen">

    <!-- jQuery Slider declarations are made in this file. The slider is found at the top of the index.html page-->
    <link rel="stylesheet" href="assets\green\css\slider.css" type="text/css" media="screen, projection">

    <!--Javascript includes, for use of the jQuery slider-->
    <!--This Template makes use of-->

    <!--jQuery-->
    <!--jQuery Slider-->
    <!--DD_BelatedPNG fix-->
    <!--CUFON text replacement-->
    <script type="text/javascript" src="assets\green\js\swfobject.js"></script>
    <script type="text/javascript" src="assets\green\js\forma.js"></script>

    <!--<script type="text/javascript" src="assets/green/js/jquery.js"></script>-->
    <script type="text/javascript" src="assets\green\js\jquery.min.js"></script>
    <script type="text/javascript" src="assets/green/js/slider.js"></script>
    <script type="text/javascript" src="assets\green\js\updatebar.js"></script>
    <!--<script type="text/javascript" src="assets/green/js/jquery.min.js"></script>-->
    <script type="text/javascript" src="assets\green\js\colorbox.js"></script>
    <script type="text/javascript">
    $(document).ready(function() {
        var tr = 0;
        $(".colorbox").colorbox();
        $(".prezent").colorbox();
        $(".inline2").colorbox({
            width: "50%",
            inline: true,
            href: "#inline_2"
        }); //our services button (feature slider)
        $(".search_pop").colorbox({
            width: "50%",
            inline: true,
            href: "#search_pop"
        }); //for the search pop-up reference
        $("#tarif").colorbox({
            width: "500",
            height: "700",
            iframe: true
        });
        $(".fot1").colorbox();
        $(".fot2").colorbox();
        $(".fot5").colorbox();
        $(".fot3").colorbox();
        $(".fot4").colorbox({
            width: "650",
            inline: true,
            href: "#fot4"
        });
        $("#login,#login1").colorbox({
            width: "550",
            onComplete: function() {
                $("#cboxTitle").html("<!-- <h3>Вход</h3><span class='in_styl'>еще не регистрировались?</span> --><span class='in_styl_r'><a href='#' onclick='webLoginShowForm(3);return false;'>регистрация</a></span>");
            }
        });
    });
    </script>

    <script type="text/javascript" src="assets\green\js\jquery.easing.js"></script>
    <script type="text/javascript" src="assets\green\js\jquery.superfish.js"></script>
    <script type="text/javascript" src="assets\green\js\jquery.hoverIntent.js"></script>
    <script type="text/javascript" src="assets\green\js\jquery.scripts.js"></script>

    <!--CUFON Text Replacement-->
    <!--This script replaces existing <h> tags with the custom Titillium Font that renders correctly in all modern browsers-->
    <!--I have included 2 different Titillium font weights, 400 and 800 respectively.-->
    <!--If you want to use the much bolder, 800 weight, simply change the "400" below to "800"-->
    <script src="assets\green\js\cufon-yui.js" type="text/javascript"></script>
    <script src="assets\green\js\TitilliumText15L_400.font.js" type="text/javascript"></script>


    <!--[if IE 6]>
        <script src="js/DD.js"></script>
        <script>
        DD_belatedPNG.fix('#leftArrow, #rightArrow, img');
        </script>
        <![endif]-->
    <script type="text/javascript">
    var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
    document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
    </script>
    <script type="text/javascript">
    try {
        var pageTracker = _gat._getTracker("UA-6952644-1");
        pageTracker._trackPageview();
    } catch (err) {}
    </script>
    <!-----------------------------------!!!!!!!!!!!!!!!!-------------------------------------------------------------->



    <?php $this->head() ?>
</head>

<body>
    <div class="container">
        <!--#BEGIN HEADER#-->
        <!--Declare 950px width as class-->
        <div id="header" class="span-24">

            <!--Declare 470px width for logo-->
            <div class="span-12">
                <!--#LOGO#-->
                <a href="index.html"><img src="assets\green\images\logo.png" alt=""></a>
            </div>

            <!--#BEGIN NAVIGATION#-->
            <!-- Without declaring this span-24, stupid IE 6 sees too much space below the nav bar. fun times. -->
            <div class="span-24">
                <!--Main Navigation Menu-->
                <div id="menu">

                    <script type='text/javascript'>
                    $(document).ready(function() {
                        $('#text_menu').html('Сервис <span class=color>дистанционного веб-интервью</span> на должность в Вашей компании. </span>');
                    });
                    </script>
                    <?php if (Yii::$app->user->isGuest): ?>
                        <ul>
                            <li class='user'><a id="signin" href='javascript:void(0)' id='login' title=''>Вход</a></li>
                        </ul>
                    <?php else: ?>
                        <?php $form = ActiveForm::begin(['action' => Yii::getAlias('@web') . '/site/logout', 'id' => 'form_logout']); ?>
                            <?=  Html::hiddenInput(\Yii :: $app->getRequest()->csrfParam, \Yii :: $app->getRequest()->getCsrfToken(), []); ?>
                            <ul>
                                <li class='user'><?= Html::a(Html::encode('Выход'), 'javascript:;', ['id' => 'logout']) ?></li>
                            </ul>
                        <?php ActiveForm::end(); ?>
                    <?php endif; ?>
                    <ul>
                        <li class="uslov last"><a href="conditions.html" title="Условия использования сервиса Lime Prime">Условия</a></li>
                        <!--<li  class="uslov last"><a href="#" title="Условия использования сервиса Lime Prime">Условия</a></li>-->
                    </ul>
                    <ul>
                        <li class="home active"><a href="index.html" title="Lime-Prime. Сервис дистанционного подбора, оценки, аттестации персонала с помощью видео-интервью.">Главная</a></li>
                        <!--<li  class="home active"><a href="#" title="Lime-Prime. Сервис дистанционного подбора, оценки, аттестации персонала с помощью видео-интервью.">Главная</a></li>-->
                        <li class="servis"><a href="features.html" title="Как пользоваться сервисом?">Как пользоваться?</a></li>
                        <!--<li  class="servis"><a href="#" title="Как пользоваться сервисом?">Как пользоваться?</a></li>-->
                    </ul>



                    <!--This text is found in the navigation bar to the left of the linked buttons.-->
                    <div class="menu_text" id="text_menu">
                        <!--This is an IE6 workaround for problems rendering jquery sliding text-overs.-->
                        <!--WIthout this fix, IE6 renders "Visit Site" outside of the last image-->
                        <!--[if IE 6]><div class="fix6"><br><a href="http://themeforest.net/">Visit Site</a></div><![endif]-->

                    </div>
                    <!--menu text close-->

                </div>
                <!--menu close-->
                <div class="box_bg_900"></div>
            </div>
            <!--span-24 close-->

        </div> <!-- end header-->
    </div>
    <!--container close-->
    <?php $this->beginBody() ?>

    <?= $content ?>

    <?php $this->endBody() ?>
</body>
<script type="text/javascript">
    $(document).ready(function(){
        $("#signin").click(function(){
            event.preventDefault();
            $.ajax({
                type: "GET",
                url: "frontend/web/site/login",
                success: function(data) {
                    $("body").append(data);
                },
                error: function(xhr, str) {
                
                }
            });
        });

        $("#logout").click(function(){
            event.preventDefault();
            $('#form_logout').submit();
        });
    });
</script>
</html>
<?php $this->endPage() ?>