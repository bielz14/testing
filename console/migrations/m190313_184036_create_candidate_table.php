<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%candidate}}`.
 */
class m190313_184036_create_candidate_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%candidate}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'email' => $this->string()->notNull(),
            'list_id' => $this->integer(),
            'created_at' => $this->string()->notNull(),
            'updated_at' => $this->string(),
            'video_url' => $this->string(),
            'author_id' => $this->integer(),
            'invited' => $this->boolean()->defaultValue(false),
            'invited_date' => $this->string(),
            'tested' => $this->boolean()->defaultValue(false)
        ], $tableOptions);

        $this->createIndex(
            'idx-candidate-list_id',
            'candidate',
            'list_id'
        );

        $this->addForeignKey(
            'fk-candidate-list_id',
            'candidate',
            'list_id',
            'question_list',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-candidate-author_id',
            'candidate',
            'author_id'
        );

        $this->addForeignKey(
            'fk-candidate-author_id',
            'candidate',
            'author_id',
            'user',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%candidate}}');
    }
}
