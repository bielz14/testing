<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%question_list}}`.
 */
class m190313_182620_create_question_list_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%question_list}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'author_id' => $this->integer(),

        ], $tableOptions);

        $this->createIndex(
            'idx-question_list-author_id',
            'question_list',
            'author_id'
        );

        $this->addForeignKey(
            'fk-question_list-author_id',
            'question_list',
            'author_id',
            'user',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%question_list}}');
    }
}
