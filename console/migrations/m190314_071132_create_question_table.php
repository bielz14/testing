<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%questions}}`.
 */
class m190314_071132_create_question_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%question}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'list_id' => $this->integer(),
            'position' => $this->integer(),
            'time' => $this->integer(),
        ], $tableOptions);

        $this->createIndex(
            'idx-question-list_id',
            'question',
            'list_id'
        );

        $this->addForeignKey(
            'fk-question-list_id',
            'question',
            'list_id',
            'question_list',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%question}}');
    }
}
