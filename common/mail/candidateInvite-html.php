<?php
use yii\helpers\Html;
?>
<div class="invite-candidate">
    <h2>Здравствуйте, вас приветствует ресурс lime-prime</h2>
    <div class="text">
    	<p>
    		Приглашаем пройти тестирование на нашем сайте по этой ссылке <b><span style="color: red"> &#8658; </span></b><a style="color: blue; text-decoration: none" href="<?= Yii::$app->urlManager->createAbsoluteUrl(['/']) ?>site/testing?candidate_id=<?= $candidate_id ?>">Тестирование</a>
    	</p>
    </div>
</div>
