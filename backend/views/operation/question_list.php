<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>
<div id="newquestion" class="modal-self">
  <?php $form = ActiveForm::begin(['action' => Yii::getAlias('@web') . '/operation/addquestionlist', 'id' => 'question-list-form', 'options' => ['class' => 'modal-content animate']]); ?>
      <div class="">
        <?=  Html::hiddenInput(\Yii :: $app->getRequest()->csrfParam, \Yii :: $app->getRequest()->getCsrfToken(), []); ?>
      </div>
      <div class="">
          <?= $form->field($model, 'title')->input('text', ['name' => 'title'])->label('*Введите название списка вопросов'); ?>
          <button type="submit">Отправить</button>
      </div>

      <div class="" style="background-color:#f1f1f1">
        <button type="button" onclick="document.getElementById('newquestion').style.display='none'" class="cancelbtn">Cancel</button>
      </div>
  <?php ActiveForm::end(); ?>
</div>