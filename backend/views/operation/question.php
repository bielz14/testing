<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>
<div id="newquestion" class="modal-self">
  <?php $form = ActiveForm::begin(['action' => Yii::getAlias('@web') . '/operation/addquestion', 'id' => 'question-form', 'options' => ['class' => 'modal-content animate']]); ?>
      <!--<div class="imgcontainer">
        <span onclick="document.getElementById('newquestion').style.display='none'" class="close" title="Close Modal">&times;</span>
      </div>-->
      <div class="">
        <?=  Html::hiddenInput(\Yii :: $app->getRequest()->csrfParam, \Yii :: $app->getRequest()->getCsrfToken(), []); ?>
      </div>
      <div class="">
        <?=  Html::hiddenInput('list_id', $listId); ?>
      </div>
      <div class="">
          <?= $form->field($model, 'title')->input('text', ['name' => 'title'])->label('*Введите название вопроса'); ?>
          <?= $form->field($model, 'position')->input('text', ['name' => 'position'])->label('*Введите позицию вопроса в списке'); ?>
          <?= $form->field($model, 'time')->input('text', ['name' => 'time'])->label('*Введите время отведенное на вопрос в секундах'); ?>
          <button type="submit">Отправить</button>
      </div>

      <div class="" style="background-color:#f1f1f1">
        <button type="button" onclick="document.getElementById('newquestion').style.display='none'" class="cancelbtn">Cancel</button>
      </div>
  <?php ActiveForm::end(); ?>
</div>