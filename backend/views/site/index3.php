<!--<h2>Tabs</h2>-->

<!--<div class="tab">
  <button class="tablinks" onclick="openCity(event, 'Record')">Записать вопросы</button>
  <button class="tablinks" onclick="openCity(event, 'Add')">Добавить кандидата</button>
</div>-->
<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use frontend\assets\AppAsset;

AppAsset::register($this);
?>
<div class="container" style="margin-top: 10%; display: block; width: 55%;">
  <div class="wrapper" style="padding-left: 5.5%">
    <div style="float: left">
      <table class="table table-bordered" id="question_id_table" style="float: left">
        <button id="clear-list" type="button" class="btn btn-info" style="margin-bottom: 1%; display: block">Убрать все</button>
        <thead>
          <tr>
            <th scope="col">
              <span style="color: #138496; font-size: 150%">Списки вопросов</span>
              <div class="list_operation" style="margin-top: 1%;">
                <button id="add-list" type="button" class="btn btn-success">Добавить</button>
                <button id="delete-list" type="button" class="btn btn-danger">Удалить</button>
                <button id="update-list" type="button" class="btn btn-warning">Изменить</button>
              </div>
            </th>
          </tr>
        </thead>
        <tbody>
          <?php if (isset($questionLists)): ?>
            <?= Html::hiddenInput('question_list_ids', null, ['id' => 'question_list_ids']) ?>
            <?php foreach ($questionLists as $questionList): ?>
              <tr data-question-list-id="<?= $questionList->id; ?>">
                <td>
                  <span><?= $questionList->title; ?><span>
                  <div style="display: inline-block">
                    <?= Html::checkbox('question', false, ['class' => 'question-list', 'data-question-list-id' => $questionList->id]) ?>
                  </div>
                </td>
              </tr>
            <?php endforeach; ?>
          <?php endif; ?>
        </tbody>
      </table>
    </div>
    <div style="float: left">
      <table class="table table-bordered" id="questions_table">
        <button id="clear-question" type="button" class="btn btn-info" style="margin-bottom: 1%; display: block">Убрать все</button>
        <thead>
          <tr>
            <th scope="col">
              <span style="color: #138496; font-size: 150%">Вопросы</span>
              <div class="list_operation" style="margin-top: 1%;">
                <button id="add-question" type="button" class="btn btn-success">Добавить</button>
                <button id="delete-question" type="delete-question" class="btn btn-danger">Удалить</button>
                <button id="update-question" type="update-question" class="btn btn-warning">Изменить</button>
              </div>
            </th>
          </tr>
        </thead>
        <tbody>
          <?php if (isset($questions)): ?>
            <?= Html::hiddenInput('question_ids', null, ['id' => 'question_ids']) ?>
            <?php foreach ($questions as $question): ?>
              <tr data-question-id="<?= $question->id; ?>">
                <td>
                  <span><?= $question->title; ?><span>
                  <div style="display: inline-block">
                    <?= Html::checkbox('question', false, ['class' => 'question', 'data-question-id' => $question->id]) ?>
                  </div>
                </td>
              </tr>
            <?php endforeach; ?>
          <?php endif; ?>
          <!--<tr>
            <td>
              <span>First questions list<span>
              <div style="display: inline-block">
                <input type="checkbox">
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <span>Second questions list<span>
              <div style="display: inline-block">
                <input type="checkbox">
              </div>
            </td>
          </tr>-->
        </tbody>
      </table>
    </div>
  </div>
</div>
<!--<div id="Record" class="tabcontent" style="display: block;">
  <div class="container" style="display: flex;">
    <table border="1">
      <tbody>
        <tr class="footnav" id="myFootnav">
          <th><button onclick="document.getElementById('newList').style.display='block'">Добавить список вопросов</button></th>
          <th><button onclick="document.getElementById('deleteList').style.display='block'">Удалить список</button></th>
          <th><button onclick="document.getElementById('changeList').style.display='block'">Переиминовать список</button></th>
        </tr>
        <tr>
          <td colspan="3">Список вопросов</td>
        </tr>
        <tr>
          <td colspan="3" class="btn" >
            <div class="canclick">Name of the questions list</div>
          </td>
        </tr>
      </tbody>
    </table>

    <table class="table table-bordered">
      <thead>
        <tr>
          <th scope="col">Список вопросов</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>
            <span>First questions list<span>
            <div style="display: inline-block">
              <input type="checkbox">
            </div>
          </td>
        </tr>
        <tr>
          <td>
            <span>Second questions list<span>
            <div style="display: inline-block">
              <input type="checkbox">
            </div>
          </td>
        </tr>
      </tbody>
    </table>

    <table border="1">
      <tbody>
        <tr class="footnav" id="myFootnav">
          <th><button onclick="document.getElementById('newquestion').style.display='block'">Добавить вопрос</button></th>
          <th><button onclick="document.getElementById('deletequestion').style.display='block'">Удалить вопрос</button></th>
          <th><button onclick="document.getElementById('changequestion').style.display='block'">Изменить вопрос</button></th>
        </tr>
        <tr>
          <td colspan="3">Вопрос</td>
        </tr>
        <tr>
          <td colspan="3" class="btn" >
           <div><input type="checkbox"> Name of the question</div>
          </td>
        </tr>
      </tbody>
    </table>
  </div>
</div>-->

<div id="Add" class="tabcontent">
  <div class="container" style="display: flex;">
    <table border="1">
      <tbody>
        <tr class="footnav" id="myFootnav">
          <th colspan="2"><button>Добавить</button></th>
          <th><button>Удалить</button></th>
          <th><button>Изменить</button></th>
          <th><button>Пригласить</button></th>
        </tr>
        <tr>
          <th>
            <div><input type="checkbox" name=""></div>
          </th>
          <th>
            <div>ФИО</div>
          </th>
          <th>
            <div>Список вопросов</div>
          </th>
          <th>  
            <div>Приглашение (Дата)</div>
          </th>
          <th>  
            <div>Видео</div>
          </th>
        </tr>
        <tr>
          <td>
            <div><input type="checkbox" name=""></div>
          </td>
          <td>
            <div>Aleksandr</div>
          </td>
          <td>
            <div>Name of the questions list</div>
          </td>
          <td>
            <div>01.01.2019</div>
          </td>
          <td>
            <!--<div class="canclick"><img src="img/149104.svg" width="20"></div>-->
          </td>
        </tr>
      </tbody>
    </table>
  </div>
</div>

<!--<div id="newList" class="modal-self">
  <?php //$form = ActiveForm::begin(['action' => Yii::getAlias('@web') . '/site/login', 'id' => 'login-form', 'options' => ['class' => 'modal-content animate']]); ?>
  <form class="modal-content animate" action="">
    <div class="imgcontainer">
      <span onclick="document.getElementById('newList').style.display='none'" class="close" title="Close Modal">&times;</span>
    </div>

    <div class="container">
      <span>*</span><label for="email"><b>Введите название списка вопросов</b></label>
      <input type="text" placeholder="" name="listName" required>
      <button type="submit">Отправить</button>
    </div>

    <div class="container" style="background-color:#f1f1f1">
      <button type="button" onclick="document.getElementById('newList').style.display='none'" class="cancelbtn">Cancel</button>
    </div>
  </form>
</div>

<div id="deleteList" class="modal-self">
  <form class="modal-content animate" action="">
    <div class="imgcontainer">
      <span onclick="document.getElementById('deleteList').style.display='none'" class="close" title="Close Modal">&times;</span>
    </div>

    <div class="container">
      <label for="email"><b>Вы уверены, что хотите удалить список?</b></label>
      <button type="submit">Удалить</button>
    </div>

    <div class="container" style="background-color:#f1f1f1">
      <button type="button" onclick="document.getElementById('deleteList').style.display='none'" class="cancelbtn">Cancel</button>
    </div>
  </form>
</div>

<div id="changeList" class="modal-self">
  <form class="modal-content animate" action="">
    <div class="imgcontainer">
      <span onclick="document.getElementById('changeList').style.display='none'" class="close" title="Close Modal">&times;</span>
    </div>

    <div class="container">
      <label for="email"><b>Введите новое название списка вопросов</b></label>
      <input type="text" placeholder="" name="changeName" required>
      <button type="submit">Пременить</button>
    </div>

    <div class="container" style="background-color:#f1f1f1">
      <button type="button" onclick="document.getElementById('changeList').style.display='none'" class="cancelbtn">Cancel</button>
    </div>
  </form>
</div>




<div id="newquestion" class="modal-self">
  <form class="modal-content animate" action="">
    <div class="imgcontainer">
      <span onclick="document.getElementById('newquestion').style.display='none'" class="close" title="Close Modal">&times;</span>
    </div>

    <div class="container">
      <span>*</span><label for="email"><b>Введите название вопроса</b></label>
      <input type="text" placeholder="" name="listName" required>
      <button type="submit">Отправить</button>
    </div>

    <div class="container" style="background-color:#f1f1f1">
      <button type="button" onclick="document.getElementById('newquestion').style.display='none'" class="cancelbtn">Cancel</button>
    </div>
  </form>
</div>

<div id="deletequestion" class="modal-self">
  <form class="modal-content animate" action="">
    <div class="imgcontainer">
      <span onclick="document.getElementById('deleteList').style.display='none'" class="close" title="Close Modal">&times;</span>
    </div>

    <div class="container">
      <label for="email"><b>Вы уверены, что хотите удалить вопрос?</b></label>
      <button type="submit">Удалить</button>
    </div>

    <div class="container" style="background-color:#f1f1f1">
      <button type="button" onclick="document.getElementById('deleteList').style.display='none'" class="cancelbtn">Cancel</button>
    </div>
  </form>
</div>

<div id="changequestion" class="modal-self">
  <form class="modal-content animate" action="">
    <div class="imgcontainer">
      <span onclick="document.getElementById('changequestion').style.display='none'" class="close" title="Close Modal">&times;</span>
    </div>

    <div class="container">
      <label for="email"><b>Введите текст вопрос</b></label>
      <input type="text" placeholder="" name="changeName" required>
      <button type="submit">Пременить</button>
    </div>

    <div class="container" style="background-color:#f1f1f1">
      <button type="button" onclick="document.getElementById('changequestion').style.display='none'" class="cancelbtn">Cancel</button>
    </div>
  </form>
</div>-->

<script>
function myFunction() {
  var x = document.getElementById("myTopnav");
  if (x.className === "topnav") {
    x.className += " responsive";
  } else {
    x.className = "topnav";
  }
}
</script>

<script>
function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}
</script>

<script>
  $(document).ready(function(){   
      $("body").on("click", "#add-question", function(){
           $.ajax({
              type: "GET",
              url: "/backend/web/operation/addquestion",
              data: <?= \yii\helpers\Json::encode([
                  Yii::$app->request->csrfParam => \yii::$app->request->csrfToken,
              ]) ?>,
              success: function(data) {
                $("body").append(data);
              },  
              error: function(xhr, str) {
                
              }
          });
      });

      $("body").on("click", "#delete-question", function(){
           var value = $("#question_ids").val();
           $.ajax({
              type: "POST",
              url: "/backend/web/operation/deletequestion",
              data: {'question_ids': value, '<?= Yii::$app->request->csrfParam ?>': '<?= \yii::$app->request->csrfToken ?>'},
              success: function(data) {
                if (data) {
                  value = value.replace(/^:/, '').replace(/:$/, '');
                  value = value.split('::');
                  $.each(value, function( index, value ) {
                    $('tr[data-question-id=' + value + ']').remove();
                    var replaced_value = $("#question_ids").val().replace(':' + value + ':', '');
                    $("#question_ids").val(replaced_value);
                  });
                }
              },  
              error: function(xhr, str) {
                
              }
          });
      });

      $("body").on("click", "#update-question", function(){
           if ($('.question:checked').size() > 1) {
              alert('Выберите только один вопрос!')
           } else if (!$('.question:checked').size()) {
              alert('Выберите один вопрос!')
           } else {
              var value = $("#question_ids").val();
              $.ajax({
                  type: "POST",
                  url: "/backend/web/operation/updatequestion",
                  data: {'question_ids': value, '<?= Yii::$app->request->csrfParam ?>': '<?= \yii::$app->request->csrfToken ?>'}, 
                  success: function(data) {
                    $('.question:checked').checked = false;
                    $("#question_ids").val('');
                    if (data) {
                      $("body").append(data);
                      //value = value.replace(/^:/, '').replace(/:$/, '');
                      //value = value.split('::');
                      //var text = $('tr[data-question-id="' + value + '"] > td > span').text();
                      //$("#question-title").val(text);
                    }
                  },  
                  error: function(xhr, str) {
                    
                  }
              });
           }
      });

      $(".question").change(function() {
        var value = $("#question_ids").val();
        if ($(this).is(":checked")) {     
          $("#question_ids").val(value + ':' + $(this).data('question-id') +  ':');
        } else {
          value = value.replace(':' + $(this).data('question-id') + ':', '');
          $("#question_ids").val(value);
        }
      });

      $("body").on("click", "#clear-question", function() {
        $.each($('.question:checked'), function( index, value ) {
          value.checked = false;
        });
        $("#question_ids").val('');
      });


      //////////////////////////////////////////////////////////////////////


      $("body").on("click", "#add-list", function(){
           if (!$('.question:checked').size()) {
              alert('Выберите вопросы, которые буду входить список!')
           } else {
              var value = $("#question_ids").val();
              $.ajax({
                type: "GET",
                url: "/backend/web/operation/addquestionlist",
                data: {'question_ids': value, '<?= Yii::$app->request->csrfParam ?>': '<?= \yii::$app->request->csrfToken ?>'}, 
                success: function(data) {
                  $("body").append(data);
                },  
                error: function(xhr, str) {
                  
                }
            });
           }
      });

      $("body").on("click", "#update-list", function(){
           if ($('.question-list:checked').size() > 1) {
              alert('Выберите только один список!')
           } else if (!$('.question-list:checked').size()) {
              alert('Выберите один список!')
           } else {
              var value = $("#question_list_ids").val();
              $.ajax({
                  type: "POST",
                  url: "/backend/web/operation/updatequestionlist",
                  data: {'question_list_ids': value, '<?= Yii::$app->request->csrfParam ?>': '<?= \yii::$app->request->csrfToken ?>'}, 
                  success: function(data) {
                    $('.question-list:checked').checked = false;
                    $("#question_list_ids").val('');
                    if (data) {
                      $("body").append(data);
                      //value = value.replace(/^:/, '').replace(/:$/, '');
                      //value = value.split('::');
                      //var text = $('tr[data-question-id="' + value + '"] > td > span').text();
                      //$("#question-title").val(text);
                    }
                  },  
                  error: function(xhr, str) {
                    
                  }
              });
           }
      });

      $("body").on("click", "#delete-list", function(){
           var value = $("#question_list_ids").val();
           $.ajax({
              type: "POST",
              url: "/backend/web/operation/deletequestionlist",
              data: {'question_list_ids': value, '<?= Yii::$app->request->csrfParam ?>': '<?= \yii::$app->request->csrfToken ?>'},
              success: function(data) {
                if (data) {
                  value = value.replace(/^:/, '').replace(/:$/, '');
                  value = value.split('::');
                  $.each(value, function( index, value ) {
                    $('tr[data-question-list-id=' + value + ']').remove();
                    var replaced_value = $("#question_list_ids").val().replace(':' + value + ':', '');
                    $("#question_list_ids").val(replaced_value);
                  });
                }
              },  
              error: function(xhr, str) {
                
              }
          });
      });

      $("body").on("click", "#clear-list", function() {
        $.each($('.question-list:checked'), function( index, value ) {
          value.checked = false;
        });
        $("#question_list_ids").val('');
      });

      $(".question-list").change(function() {
        var value = $("#question_list_ids").val();
        if ($(this).is(":checked")) {     
          $("#question_list_ids").val(value + ':' + $(this).data('question-list-id') +  ':');
        } else {
          value = value.replace(':' + $(this).data('question-list-id') + ':', '');
          $("#question_list_ids").val(value);
        }
      });
  });
</script>