<!DOCTYPE html>
<html lang="ru">
  <head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <title>Демонстрация getUserMedia API</title>
  <style>
  body
  {
    max-width: 500px;
    margin: 2em auto;
    font-size: 20px;
  }

  h1
  {
    text-align: center;
  }

  .buttons-wrapper
  {
    text-align: center;
  }

  .hidden
  {
    display: none;
  }

  #video
  {
    display: block;
    width: 100%;
  }

  .button-demo
  {
    padding: 0.5em;
    display: inline-block;
    margin: 1em auto;
  }

  .author
  {
    display: block;
    margin-top: 1em;
  }
  </style>
  </head>
  <body>
  <h1>Демонстрация getUserMedia API</h1>
  <video id="video" autoplay controls></video>
  <div class="buttons-wrapper">
  <button id="button-play-gum" class="button-demo" href=
http://www.pixelcom.crimea.ua/"#">Воспроизведение</button>
  <button id="button-stop-gum" class="button-demo" href=
http://www.pixelcom.crimea.ua/"#">Стоп</button>
  </div>
  <span id="gum-unsupported" class="hidden">Ваш браузер не поддерживает getUserMedia API</span>
  <span id="gum-partially-supported" class="hidden">Ваш браузер поддерживает getUserMedia API частично (только видео)</span>
<script>
  var videoStream = null;
  var video = document.getElementById("video");

  // Поддержка браузерами
  window.navigator = window.navigator || {};
  navigator.getUserMedia = navigator.getUserMedia   ||
           navigator.webkitGetUserMedia ||
           navigator.mozGetUserMedia  ||
           null;

  if (navigator.getUserMedia === null) {
    document.getElementById('gum-unsupported').classList.remove('hidden');
    document.getElementById('button-play-gum').setAttribute('disabled', 'disabled');
    document.getElementById('button-stop-gum').setAttribute('disabled', 'disabled');
  } else {
    // Опера <= 12.16 принимает direct stream.
    // Подробнее об этом здесь: http://dev.opera.com/articles/view/playing-with-html5-video-and-getusermedia-support/
    var createSrc = window.URL ? window.URL.createObjectURL : function(stream) {return stream;};

    // Опера <= 12.16 поддерживает только видео.
    var audioContext = window.AudioContext   ||
         window.webkitAudioContext ||
         null;
    if (audioContext === null) {
    document.getElementById('gum-partially-supported').classList.remove('hidden');
    }

    document.getElementById('button-play-gum').addEventListener('click', function() {
    // Захват аудио и видео с устройства пользователя
    navigator.getUserMedia({
    video: true,
    audio: true
    },
    function(stream) {
    videoStream = stream;
    // Stream the data
    video.src = createSrc(stream);
    video.play();
    },
    function(error) {
    console.log("Ошибка захвата видео: ", error.code);
    });
    });
    document.getElementById('button-stop-gum').addEventListener('click', function() {
    // Пауза
    video.pause();
    // Стоп
    videoStream.stop();
    });
  }
  </script>
</body>
</html>