<!--<h2>Tabs</h2>-->

<!--<div class="tab">
  <button class="tablinks" onclick="openCity(event, 'Record')">Записать вопросы</button>
  <button class="tablinks" onclick="openCity(event, 'Add')">Добавить кандидата</button>
</div>-->
<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use frontend\assets\AppAsset;

AppAsset::register($this);
?>
<div class="container" style="margin-top: 10%; display: block; width: 58%;">
  <div style="margin-bottom: 5%">
    <button id="question-content-btn" type="button" class="btn btn-basic" style="margin-bottom: 1%; display: inline-block">Вопросы</button>
    <button id="candidate-content-btn" type="button" class="btn btn-basic" style="margin-bottom: 1%; display: inline-block">Кандидаты</button>
  </div>
  <div class="wrapper" style="padding-left: 5.5%">
    <div class="question-list-content" style="float: left">
      <table class="table table-bordered" id="question_id_table" style="float: left">
        <button id="clear-list" type="button" class="btn btn-info" style="margin-bottom: 1%; display: block">Убрать все</button>
        <thead>
          <tr>
            <th scope="col">
              <span style="color: #138496; font-size: 150%">Списки вопросов</span>
              <div class="list_operation" style="margin-top: 1%;">
                <button id="add-list" type="button" class="btn btn-success">Добавить</button>
                <button id="delete-list" type="button" class="btn btn-danger">Удалить</button>
                <button id="update-list" type="button" class="btn btn-warning">Изменить</button>
                <button id="out-question" type="button" class="btn btn-primary">Вопросы</button>
              </div>
            </th>
          </tr>
        </thead>
        <tbody>
          <?php if (isset($questionLists)): ?>
            <?= Html::hiddenInput('question_list_ids', null, ['id' => 'question_list_ids']) ?>
            <?php foreach ($questionLists as $questionList): ?>
              <tr data-question-list-id="<?= $questionList->id; ?>">
                <td>
                  <span><?= $questionList->title; ?><span>
                  <div style="display: inline-block">
                    <?= Html::checkbox('question-list', false, ['class' => 'question-list', 'data-question-list-id' => $questionList->id]) ?>
                  </div>
                </td>
              </tr>
            <?php endforeach; ?>
          <?php endif; ?>
        </tbody>
      </table>
    </div>
    <div class="question-content" style="float: left">
      <table class="table table-bordered" id="question_table">
        <button id="clear-question" type="button" class="btn btn-info" style="margin-bottom: 1.5%;">Убрать все</button>
        <?php if (isset($questions) && count($questions)): ?>
            <div id="question-list-name" style="float: right">
              <span><b><?= $questions[0]->questionList->title ?>:</b></span>
            </div>
        <?php endif; ?>
        <thead>
          <tr>
            <th scope="col">
              <span style="color: #138496; font-size: 150%">Вопросы</span>
              <div class="list_operation" style="margin-top: 1%;">
                <button id="add-question" type="button" class="btn btn-success">Добавить</button>
                <button id="delete-question" type="delete-question" class="btn btn-danger">Удалить</button>
                <button id="update-question" type="update-question" class="btn btn-warning">Изменить</button>
              </div>
            </th>
          </tr>
        </thead>
        <tbody>
          <?php if (isset($questions)): ?>
            <?= Html::hiddenInput('question_ids', null, ['id' => 'question_ids']) ?>
            <?php foreach ($questions as $question): ?>
              <tr data-question-id="<?= $question->id; ?>">
                <td>
                  <span><?= $question->position; ?>.&nbsp</span>
                  <span><?= $question->title; ?></span>
                  <span>&nbsp(<?= $question->time; ?> сек.)</span>
                  <div style="display: inline-block">
                    <?= Html::checkbox('question', false, ['class' => 'question', 'data-question-id' => $question->id]) ?>
                  </div>
                </td>
              </tr>
            <?php endforeach; ?>
          <?php endif; ?>
        </tbody>
      </table>
    </div>
    <div class="candidate-content">
      <table class="table table-bordered" id="candidate_table">
        <button id="clear-candidate" type="button" class="btn btn-info" style="margin-bottom: 1%; display: block">Убрать все</button>
        <thead>
          <tr>
            <th scope="col" style="">
              <span style="color: #138496; font-size: 150%">Кандидаты</span>
              <div class="candidate_operation" style="margin-top: 1%;">
                <button id="add-candidate" type="button" class="btn btn-success" style="margin: 1%">Добавить</button>
                <button id="delete-candidate" type="delete-candidate" class="btn btn-danger" style="margin: 1% 1% 1% 9%">Удалить</button>
                <button id="update-candidate" type="update-candidate" class="btn btn-warning" style="margin: 1%">Изменить</button>
                <button id="invite-candidate" type="invite-candidate" class="btn btn-primary" style="margin: 1%">Пригласить</button>
              </div>
            </th>
          </tr>
          <tr>
            <th>
              Имя
            </th>
            <th>
              Email
            </th>
            <th>
              Список вопросов
            </th>
            <th>
              Дата приглашения
            </th>
            <th>
              Видео
            </th>
            <th>
              
            </th>
          </tr>
        </thead>
        <tbody>
          <?php if (isset($candidates)): ?>
            <?= Html::hiddenInput('candidate_ids', null, ['id' => 'candidate_ids']) ?>
            <?php foreach ($candidates as $candidate): ?>
              <tr data-candidate-id="<?= $candidate->id; ?>">
                <td>
                  <span><?= $candidate->name; ?><span>
                </td>
                <td>
                  <span><?= $candidate->email; ?><span>
                </td>
                <td>
                  <?php if ($candidate->questionList): ?>
                    <span><?= $candidate->questionList->title; ?><span>
                  <?php endif; ?>
                </td>
                <td>
                  <span><?= $candidate->invited_date; ?><span>
                </td>
                <td>
                  <?php if ($candidate->video_url): ?>
                    <a href="<?= $candidate->video_url; ?>">ссылка</a>
                  <?php endif; ?>
                </td>
                <td>
                  <div style="display: inline-block">
                    <?= Html::checkbox('candidate', false, ['class' => 'candidate', 'data-candidate-id' => $candidate->id]) ?>
                  </div>
                </td>
              </tr>
            <?php endforeach; ?>
          <?php endif; ?>
        </tbody>
      </table>
    </div>
  </div>
</div>

<script>
function myFunction() {
  var x = document.getElementById("myTopnav");
  if (x.className === "topnav") {
    x.className += " responsive";
  } else {
    x.className = "topnav";
  }
}
</script>
<script>
function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}
</script>

<script>
  $(document).ready(function(){   

      <?php if (Yii::$app->request->get('list_id')): ?>
        var list_id = <?= Yii::$app->request->get('list_id') ?>;
        $("#question_list_ids").val(':' + list_id + ':');
        $("input[data-question-list-id=" + list_id + "]").prop('checked', true);
      <?php endif; ?>  

      <?php if (Yii::$app->request->get('candidates')): ?>
        $(".candidate-content").show();
        $(".question-list-content").hide();
        $(".question-content").hide();
      <?php endif; ?>  

      $("body").on("click", "#question-content-btn", function(){
        $(".candidate-content").hide();
        $(".question-list-content").show();
        $(".question-content").show();
      });

      $("body").on("click", "#candidate-content-btn", function(){
        $(".question-content").hide();
        $(".question-list-content").hide();
        $(".candidate-content").show();
      });

      $("body").on("click", "#add-question", function(){
        if ($('.question-list:checked').length > 1) {
          alert('Выберите только лишь один список!'); 
        } else if (!$('.question-list:checked').length) {
          alert('Выберите один список в который будет входить новый вопрос!');
        } else {
          var value = $("#question_list_ids").val();
          value = value.replace(/^:/, '').replace(/:$/, '');
          value = value.split('::')[0];
          $.ajax({
              type: "GET",
              url: "/backend/web/operation/addquestion",
              data: {'list_id': value, '<?= Yii::$app->request->csrfParam ?>': '<?= \yii::$app->request->csrfToken ?>'}, 
              success: function(data) {
                $("body").append(data);
              },  
              error: function(xhr, str) {
                
              }
          });
        }
      });

      $("body").on("click", "#delete-question", function(){
          if ($('.question:checked').length) {
             var value = $("#question_ids").val();
             $.ajax({
                type: "POST",
                url: "/backend/web/operation/deletequestion",
                data: {'question_ids': value, '<?= Yii::$app->request->csrfParam ?>': '<?= \yii::$app->request->csrfToken ?>'},
                success: function(data) {
                  if (data) {
                    value = value.replace(/^:/, '').replace(/:$/, '');
                    value = value.split('::');
                    $.each(value, function( index, value ) {
                      console.log($('tr[data-question-id=' + value + ']'));
                      $('tr[data-question-id=' + value + ']').remove();
                      var replaced_value = $("#question_ids").val().replace(':' + value + ':', '');
                      $("#question_ids").val(replaced_value);
                    });
                  }
                },  
                error: function(xhr, str) {
                  
                }
            });
          } else {
            alert('Выберите хотя бы один вопрос!');
          }
      });

      $("body").on("click", "#update-question", function(){
           if ($('.question:checked').length > 1) {
              alert('Выберите только лишь один вопрос!');
           } else if (!$('.question:checked').length) {
              alert('Выберите один вопрос!');
           } else {
              var value = $("#question_ids").val();
              $.ajax({
                  type: "POST",
                  url: "/backend/web/operation/updatequestion",
                  data: {'question_ids': value, '<?= Yii::$app->request->csrfParam ?>': '<?= \yii::$app->request->csrfToken ?>'}, 
                  success: function(data) {
                    $('.question:checked').checked = false;
                    $("#question_ids").val('');
                    if (data) {
                      $("body").append(data);
                      //value = value.replace(/^:/, '').replace(/:$/, '');
                      //value = value.split('::');
                      //var text = $('tr[data-question-id="' + value + '"] > td > span').text();
                      //$("#question-title").val(text);
                    }
                  },  
                  error: function(xhr, str) {
                    
                  }
              });
           }
      });

      $("body").on("change", ".question", function() {
        var value = $("#question_ids").val();
        if ($(this).is(":checked")) {     
          $("#question_ids").val(value + ':' + $(this).data('question-id') +  ':');
        } else {
          value = value.replace(':' + $(this).data('question-id') + ':', '');
          $("#question_ids").val(value);
        }
      });

      $("body").on("click", "#clear-question", function() {
        $.each($('.question:checked'), function( index, value ) {
          value.checked = false;
        });
        $("#question_ids").val('');
      });


      //////////////////////////////////////////////////////////////////////


      $("body").on("click", "#add-list", function(){
        $.ajax({
          type: "GET",
          url: "/backend/web/operation/addquestionlist",
          data: {'<?= Yii::$app->request->csrfParam ?>': '<?= \yii::$app->request->csrfToken ?>'}, 
          success: function(data) {
            $("body").append(data);
          },  
          error: function(xhr, str) {
                  
          }
        });
      });

      $("body").on("click", "#update-list", function(){
           if ($('.question-list:checked').length > 1) {
              alert('Выберите только один список!')
           } else if (!$('.question-list:checked').length) {
              alert('Выберите один список!')
           } else {
              var value = $("#question_list_ids").val();
              $.ajax({
                  type: "POST",
                  url: "/backend/web/operation/updatequestionlist",
                  data: {'question_list_ids': value, '<?= Yii::$app->request->csrfParam ?>': '<?= \yii::$app->request->csrfToken ?>'}, 
                  success: function(data) {
                    $('.question-list:checked').checked = false;
                    $("#question_list_ids").val('');
                    if (data) {
                      $("body").append(data);
                      //value = value.replace(/^:/, '').replace(/:$/, '');
                      //value = value.split('::');
                      //var text = $('tr[data-question-id="' + value + '"] > td > span').text();
                      //$("#question-title").val(text);
                    }
                  },  
                  error: function(xhr, str) {
                    
                  }
              });
           }
      });

      $("body").on("click", "#delete-list", function(){
           if ($('.question-list:checked').length) {
             var value = $("#question_list_ids").val();
             $.ajax({
                type: "POST",
                url: "/backend/web/operation/deletequestionlist",
                data: {'question_list_ids': value, '<?= Yii::$app->request->csrfParam ?>': '<?= \yii::$app->request->csrfToken ?>'},
                success: function(data) {
                  if (data) {
                    value = value.replace(/^:/, '').replace(/:$/, '');
                    value = value.split('::');
                    $.each(value, function( index, value ) {
                      $('tr[data-question-list-id=' + value + ']').remove();
                      var replaced_value = $("#question_list_ids").val().replace(':' + value + ':', '');
                      $("#question_list_ids").val(replaced_value);
                    });
                  }
                },  
                error: function(xhr, str) {
                  
                }
            });
          } else {
            alert('Выберите хотя бы один список!');
          }
      });

      $("body").on("click", "#out-question", function() {
        if ($('.question-list:checked').length > 1) {
          alert('Выберите только один список для вывода его вопросов!');
        } else if (!$('.question-list:checked').length) {
          alert('Выберите один список для вывода его вопросов!');
        } else {
          var value = $("#question_list_ids").val();
          value = value.replace(/^:/, '').replace(/:$/, '');
          value = value.split('::');
          $.ajax({
            type: "GET",
            url: "/backend/web/operation/getquestionstolist",
            data: {'question_list_id': value, '<?= Yii::$app->request->csrfParam ?>': '<?= \yii::$app->request->csrfToken ?>'}, 
            success: function(data) {
              if (data) {
                var list_title = $("tr[data-question-list-id=" + value + "] > td > span:first-child")
                                            .text().replace(/^\s*(.*)\s*$/, '$1');
                var content_title = '<div id="question-list-name" style="float: right">' +
                                      '<span><b>' + list_title + ':</b></span>' +
                                    '</div>';
                $("#question-list-name").remove();
                $("#clear-question").after(content_title);
                var content = '<input type="hidden" id="question_ids" name="question_ids">';
                $("#question_table > tbody").html('');
                $("#question_table > tbody").append(content);
                $.each(JSON.parse(data), function( index, value ) {
                  content = '<tr data-question-id="' + value.id + '">' +
                              '<td>' +
                                '<span>' + value.title + '</span>' +
                                '<div style="display: inline-block; margin-left: 1.5%">' +
                                '<input type="checkbox" id="question" class="question" name="question" data-question-id="' + value.id + '">' +
                                '</div>' +
                              '</td>' +
                            '</tr>';         
                  $("#question_table > tbody").append(content);
                });
                $(".question-content").show();
              } else {
                alert('У выбранного списка отсутствуют вопросы!');
              }
            },  
            error: function(xhr, str) {
                    
            }
          });
        }
      });


      $("body").on("click", "#clear-list", function() {
        $.each($('.question-list:checked'), function( index, value ) {
          value.checked = false;
        });
        $("#question_list_ids").val('');
      });

      $("body").on("change", ".question-list", function() {
        var value = $("#question_list_ids").val();
        if ($(this).is(":checked")) {     
          $("#question_list_ids").val(value + ':' + $(this).data('question-list-id') +  ':');
        } else {
          value = value.replace(':' + $(this).data('question-list-id') + ':', '');
          $("#question_list_ids").val(value);
        }
      });


      //////////////////////////////////////////////////////////////////////


      $("body").on("click", "#add-candidate", function(){
        $.ajax({
          type: "GET",
          url: "/backend/web/operation/addcandidate",
          data: {'<?= Yii::$app->request->csrfParam ?>': '<?= \yii::$app->request->csrfToken ?>'}, 
          success: function(data) {
            $("body").append(data);
          },  
          error: function(xhr, str) {
                  
          }
        });
      });

      $("body").on("click", "#delete-candidate", function(){
           if ($('.candidate:checked').length) {
            var value = $("#candidate_ids").val();
            $.ajax({
                type: "POST",
                url: "/backend/web/operation/deletecandidate",
                data: {'candidate_ids': value, '<?= Yii::$app->request->csrfParam ?>': '<?= \yii::$app->request->csrfToken ?>'},
                success: function(data) {
                  if (data) {
                    value = value.replace(/^:/, '').replace(/:$/, '');
                    value = value.split('::');
                    $.each(value, function( index, value ) {
                      $('tr[data-candidate-id=' + value + ']').remove();
                      var replaced_value = $("#candidate_ids").val().replace(':' + value + ':', '');
                      $("#candidate_ids").val(replaced_value);
                    });
                  }
                },  
                error: function(xhr, str) {
                  
                }
            });
           } else {
            alert('Выберите хотя бы одного кандидата!');
           }
           var value = $("#candidate_ids").val();
      });

      $("body").on("click", "#update-candidate", function(){
           if ($('.candidate:checked').length > 1) {
              alert('Выберите только лишь одного кандидата!');
           } else if (!$('.candidate:checked').length) {
              alert('Выберите одного кандидата!');
           } else {
              var value = $("#candidate_ids").val();
              $.ajax({
                  type: "POST",
                  url: "/backend/web/operation/updatecandidate",
                  data: {'candidate_ids': value, '<?= Yii::$app->request->csrfParam ?>': '<?= \yii::$app->request->csrfToken ?>'}, 
                  success: function(data) {
                    $('.candidate:checked').checked = false;
                    $("#candidate_ids").val('');
                    if (data) {
                      $("body").append(data);
                      //value = value.replace(/^:/, '').replace(/:$/, '');
                      //value = value.split('::');
                      //var text = $('tr[data-question-id="' + value + '"] > td > span').text();
                      //$("#question-title").val(text);
                    }
                  },  
                  error: function(xhr, str) {
                    
                  }
              });
           }
      });

      $("body").on("click", "#clear-candidate", function() {
        $.each($('.candidate:checked'), function( index, value ) {
          value.checked = false;
        });
        $("#candidate_ids").val('');
      });

      $("body").on("change", ".candidate", function() {
        var value = $("#candidate_ids").val();
        if ($(this).is(":checked")) {     
          $("#candidate_ids").val(value + ':' + $(this).data('candidate-id') +  ':');
        } else {
          value = value.replace(':' + $(this).data('candidate-id') + ':', '');
          $("#candidate_ids").val(value);
        }
      });

      $(document).ready(function(){
        $("body").on("click", "#invite-candidate", function(){
          if ($('.candidate:checked').length) {
              var value = $("#candidate_ids").val();
              $.ajax({
                type: "POST",
                url: "/backend/web/operation/invitecandidate",
                data: {'candidate_ids': value, '<?= Yii::$app->request->csrfParam ?>': '<?= \yii::$app->request->csrfToken ?>'}, 
                success: function(data) {
                  $.each($('.candidate:checked'), function( index, value ) {
                    value.checked = false;
                  });
                  $("#candidate_ids").val('');
                  if (!data) {
                    alert('Ни один из кандидатов не был приглашен по техническим причинам!');
                  } else {
                    $(".inviting-candidates-content").remove();
                 
                    var content = '<div class="inviting-candidates-content" style="margin-top: 2%">' +
                                    '<table class="table table-bordered" id="question_id_table" style="float: left">' +
                                      '<thead>' +
                                        '<caption style="color: #13968d; font-size: 285%">Успешно приглашенные кандидаты</caption>' +
                                        '<tr>' +
                                          '<th scope="col">' +
                                            '<span>id</span>' +
                                          '</th>' +
                                          '<th scope="col">' +
                                            '<span>Имя</span>' +
                                          '</th>' +
                                        '</tr>' +
                                      '</thead>' +
                                      '<tbody>';
                    $.each(JSON.parse(data), function( index, value ) {
                        content += '<tr data-question-list-id="<?= $questionList->id; ?>">' +
                                    '<td>' +
                                      '<span>' + index + '<span>' +
                                    '</td>' +
                                    '<td>' +
                                      '<span>' + value + '<span>' +
                                    '</td>' +
                                   '</tr>';
                    });
                    content +=   '</tbody>' +
                                '</table>' +
                               '</div>';
                    $(".wrapper").append(content);
                    $('html, body').animate({ scrollTop: $(".inviting-candidates-content").offset().top }, 500);
                  }
                },  
                error: function(xhr, str) {
                            
                }
              });
          } else {
            alert('Выберите хотя бы одного кандидата!');
          }
        });
      });
      /*$("body").on("click", "#invite-candidate", function(){
        if ($('.candidate:checked').length > 1) {
            alert('Выберите только лишь одного кандидата!');
        } else if (!$('.candidate:checked').length) {
            alert('Выберите одного кандидата!');
        } else {
          var value = $("#candidate_ids").val();
          $.ajax({
            type: "POST",
            url: "/backend/web/operation/invitecandidate",
            data: {'candidate_ids': value, '<?= Yii::$app->request->csrfParam ?>': '<?= \yii::$app->request->csrfToken ?>'}, 
            success: function(data) {
              $.each($('.candidate:checked'), function( index, value ) {
                value.checked = false;
              });
              $("#candidate_ids").val('');
              if (data == 0) {
                alert('Кандидат уже был приглашен ранее!');
              } else if (data == 1) {
                alert('Кандидат успешно приглашен!');
              }  else if (data == 3) {
                alert('Кандидат не приглашен, так как, отсутствуют вопросы в его списке!');
              } else {
                alert('Кандидат не приглашен по техническим причинам!');
              }
            },  
            error: function(xhr, str) {
                        
            }
          });
        }
      });*/
  });
</script>