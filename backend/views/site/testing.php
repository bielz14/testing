<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<?php if (isset($testing) && $testing): ?>
  <div class="wrapper">
    <div class="content" style="display: block; width: auto; margin: 7.5% 0 0 11%">
      <span style="color: #3CB371; margin: 15% 0 0 15%; font-size: 200%"><b>Вы уже использовали попытку пройти тест!</b></span>
    </div>
  </div>
<?php elseif (isset($testing) && !$testing): ?>
  <div class="wrapper">
    <div class="content" style="display: block; width: auto; margin: 7.5% 0 0 20%">
      <span style="color: #3CB371; margin: 15% 10% 0 0; font-size: 200%"><b>Тестирование невозможно пройти, так как, кандидат не определен!</b></span>
    </div>
  </div>
<?php elseif (!isset($testing)): ?>
  <style>
    .question {
      display: none;
    }
    .question.active {
      display: block;
    }

    .active {
      background: none;
      color: black;
    }
  </style>
  <div class="wrapper">
    <div class="content" style="display: block; width: auto; margin: 7.5% 0 0 20%">
      <?php if (isset($questions) && count($questions)): ?>
        <div class="warning-message" style="margin-top: 2%; color: #E9967A">
          <p>Время отведенное на ответы будет ограничено таймером, у вас будет лишь одна попытка пройти тестирование, так же не забудьте убедититься в работоспособности веб-камеры и микрофона</p>
        </div>
        <button id="start" type="button" class="btn btn-success" style="margin-bottom: 0.5%;">Старт</button>
        <div class="timer" style="display: inline-block; margin-left: 1%">
        </div>
        <div id="list-name" style="display: block; margin: 1% 0 1% 0; font-size: 220%; color: #5F9EA0; display: inline-block;">
          <?php if (current($questions)->questionList): ?>
              <span><?= current($questions)->questionList->title; ?><span>
          <?php endif; ?>
        </div>
        <?php $i = 1; ?>
        <?php foreach ($questions as $question): ?>
          <div class="question" data-question-id="<?= $question->id ?>" data-question-number="<?= $i ?>" style="margin-top: 1.5%" data-time="<?= $question->time ?>">
            <div class="number-title">
              <span style="color: blue">Вопрос №<?= $i ?>:</span>
            </div>
            <div class="text">
              <span><?= $question->title ?></span>
            </div>
          </div>
          <?php $i++; ?>
        <?php endforeach; ?>
        <div class="video-webcam">
          <video id="myVideo" class="video-js vjs-default-skin"></video>
        </div>
      <?php else: ?>
        <div class="">
              <span style="color: #3CB371; margin: 15% 0 0 -12.5%; font-size: 200%"><b>Вопросы для теста были удалены по каким-то причинам, зайдите позже</b></span>
        </div>
      <?php endif; ?>
    </div>
  </div>

  <script>
      $(document).ready(function(){
        $(".vjs-icon-av-perm").hide();
      });

      var urlParams = new URLSearchParams(window.location.search);
      var candidate_id = urlParams.get('candidate_id');
      var refreshTimeoutId;
      var refreshIntervalId;
      var i = $(".question[data-question-number=1]").data('time');
      $("body").on("click", "#start", function() {
        $.ajax({
          type: "POST",
          url: "/backend/web/operation/setcandidatetested",
          data: {'candidate_id': candidate_id, '<?= Yii::$app->request->csrfParam ?>': '<?= \yii::$app->request->csrfToken ?>'},
          success: function(data) {
            if (!data) {
              alert('Тестирование не может быть начато по техническим причинам, повторите попытку позже'); 
            } else {
              $(".warning-message").remove();
              $(".vjs-duration").remove();
              $(".vjs-time-divider").remove();
              $(".vjs-icon-record-start").hide();
              $("#list-name").css("display", "block");
              $(".timer").html('Таймер: ' + '<span style="color: red">' + $(".question[data-question-number=1]").data('time') + '</span>');
              refreshIntervalId = setInterval(function() {
                i--;
                $(".timer").html('Таймер: ' + '<span style="color: red">' + i + '</span>');
              }, 1000)
              var questionCount = <?= count($questions) ?>;
              if (questionCount > 1) {
                setTimeout(function(){
                  $(".vjs-icon-record-start").click();
                  $(".vjs-icon-record-stop").hide();
                }, 300);
                var button = document.createElement('button');
                button.id = 'next';
                button.className = 'btn btn-warning';
                button.innerHTML = 'Следующий вопрос';
                $("#start").replaceWith(button);
                $(".question[data-question-number=1]").toggle('action');
                refreshTimeoutId = setTimeout(nextQuestion, $(".question[data-question-number=1]").data('time') * 1000 + 1000, 1);
                $(".vjs-icon-av-perm").click();
              } else { 
                nextQuestion(0);
                $("#start").remove();
                $(".vjs-icon-av-perm").click();
                $(".timer").css("margin-left", "0");
              }
            }
          },  
          error: function(xhr, str) {
                      
          }
        });
      });

      function nextQuestion(prevQuestionNumber) { 
        var question_time = $(".question[data-question-number=" + (prevQuestionNumber + 1) + "]").data('time');
        this.i = question_time + 1;
        var questionCount = <?= count($questions) ?>;
        if ((prevQuestionNumber + 1) < questionCount) {
            alert('Внимание, следующий вопрос!')
            $(".question[data-question-number=" + prevQuestionNumber + "]").toggle('action');
            $(".question[data-question-number=" + (prevQuestionNumber + 1) + "]").toggle('action');
            refreshTimeoutId = setTimeout(nextQuestion, question_time * 1000, prevQuestionNumber + 1);
        } else if ((prevQuestionNumber + 1) == questionCount || prevQuestionNumber == 0) {
            if (prevQuestionNumber > 1) {
              alert('Внимание, последний вопрос!');
            } else {
              alert('Внимание, список состоит только из одного вопроса!');
              setTimeout(function(){
                $(".vjs-icon-record-start").click();
                $(".vjs-icon-record-stop").hide();
              }, 300);
            }
            
            var button = document.createElement('button');
            button.id = 'stop-testing';
            button.className = 'btn btn-primary';
            button.innerHTML = 'Завершить тестирование';
            if ($("#next").length > 0) {
              $("#next").replaceWith(button);
            } else {
              $(".content").prepend(button);
            }
            $(".question[data-question-number=" + prevQuestionNumber + "]").toggle('action');
            $(".question[data-question-number=" + (prevQuestionNumber + 1) + "]").toggle('action');
            refreshTimeoutId = setTimeout(function(){
              clearInterval(refreshIntervalId);
              $(".vjs-icon-record-stop").click();
              $(".content").html('<span style="color: #3CB371; margin: 15% 0 0 15%; font-size: 200%"><b>Тестирование завершено!</b></span>');
            }, question_time * 1000);
        }
      }

      $("body").on("click", "#next", function() {
        var prevQuestionNumber = $(".question:visible").index() - 2;
        clearTimeout(refreshTimeoutId);
        nextQuestion(prevQuestionNumber);
      });

      $("body").on("click", "#stop-testing", function() {
          clearTimeout(refreshTimeoutId);
          clearInterval(refreshIntervalId);
          $(".vjs-icon-record-stop").click();
          $(".content").html('<span style="color: #3CB371; margin: 15% 0 0 15%; font-size: 200%"><b>Тестирование завершено!</b></span>');
      });
  </script>
          <script src="https://unpkg.com/video.js@6.11.0/dist/video.min.js"></script>
          <script src="https://cdn.webrtc-experiment.com/RecordRTC.js"></script>
          <script src="https://webrtc.github.io/adapter/adapter-latest.js"></script>
          <script src="https://cdnjs.cloudflare.com/ajax/libs/videojs-record/3.5.1/videojs.record.js"></script>
          <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
          <script>

            var head = document.getElementsByTagName('head')[0];

            var link = document.createElement('link');
            link.rel = 'stylesheet';
            link.href = "https://unpkg.com/video.js@6.11.0/dist/video-js.min.css";
            head.appendChild(link);

            var link = document.createElement('link');
            link.rel = 'stylesheet';
            link.href = "https://cdnjs.cloudflare.com/ajax/libs/videojs-record/3.5.1/css/videojs.record.css";
            head.appendChild(link);

            var videoMaxLengthInSeconds = 120;
            var player = videojs("myVideo", {
                controls: true,
                width: 720,
                height: 480,
                fluid: false,
                plugins: {
                    record: {
                        audio: true,
                        video: true,
                        maxLength: videoMaxLengthInSeconds,
                        debug: true,
                        videoMimeType: "video/webm;codecs=H264"
                    }
                }
            }, function(){
                videojs.log(
                    'Using video.js', videojs.VERSION,
                    'with videojs-record', videojs.getPluginVersion('record'),
                    'and recordrtc', RecordRTC.version
                );
            });

            player.on('deviceError', function() {
                console.log('device error:', player.deviceErrorCode);
            });

            player.on('error', function(error) {
                console.log('error:', error);
            });

            player.on('startRecord', function() {
                console.log('started recording! Do whatever you need to');
            });

            player.on('finishRecord', function() {
                console.log('finished recording: ', player.recordedData);

                var formData = new FormData();
                formData.append('video', player.recordedData);
                formData.append('<?= Yii::$app->request->csrfParam ?>', '<?= \yii::$app->request->csrfToken ?>');
                formData.append('candidate_id', candidate_id);

                $.ajax({
                    type: "POST",
                    url: "/backend/web/operation/uploadvideo",
                    data: formData,
                    cache: false,
                    dataType: 'blob',
                    processData: false,
                    contentType: false, 
                    success: function(data) {
                      $(".video-webcam").remove();
                    },  
                    error: function(xhr, str) {
                      
                    }
                });
            });
          </script>
<?php endif; ?>
