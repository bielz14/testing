<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>
<style>
  .question {
    display: none;
  }
  .question.active {
    display: block;
  }

  .active {
    background: none;
    color: black;
  }
</style>
<div class="wrapper">
  <div class="content" style="display: block; width: auto; margin: 15% 0 0 38%">
    <button id="start" type="button" class="btn btn-success">Старт</button>
    <div class="timer" style="display: inline-block;">
    </div>
    <?php if (isset($questions)): ?>
      <?php $i = 1; ?>
      <?php foreach ($questions as $question): ?>
        <div class="question" data-question-id="<?= $question->id ?>" data-question-number="<?= $i ?>" style="margin-top: 2.5%">
          <div class="number-title">
            <span style="color: blue">Вопрос №<?= $i ?>:</span>
          </div>
          <div class="text">
            <span><?= $question->title ?></span>
          </div>
        </div>
        <?php $i++; ?>
      <?php endforeach; ?>
    <?php endif; ?>
  </div>
</div>

<script>
  var refreshTimeoutId;
  var refreshIntervalId;
  var i = 10;
  $("body").on("click", "#start", function() {
    var button = document.createElement('button');
    button.id = 'next';
    button.className = 'btn btn-warning';
    button.innerHTML = 'Следующий вопрос';
    this.replaceWith(button);
    $(".timer").html('Таймер: ' + '<span style="color: red">10</span>');
    refreshIntervalId = setInterval(function() {
      i--;
      $(".timer").html('Таймер: ' + '<span style="color: red">' + i + '</span>');
    }, 1000)
    $(".question[data-question-number=1]").toggle('action');
    refreshTimeoutId = setTimeout(nextQuestion, 10000, 1);
  });

  function nextQuestion(prevQuestionNumber) {
    this.i = 10;
    $(".timer").html('Таймер: ' + '<span style="color: red">10</span>');
    var questionCount = <?= count($questions) ?>;
    if ((prevQuestionNumber + 1) < questionCount) {
        alert('Внимание, следующий вопрос!')
        $(".question[data-question-number=" + prevQuestionNumber + "]").toggle('action');
        $(".question[data-question-number=" + (prevQuestionNumber + 1) + "]").toggle('action');
        refreshTimeoutId = setTimeout(nextQuestion, 10000, prevQuestionNumber + 1);
    } else if ((prevQuestionNumber + 1) == questionCount) {
        alert('Внимание, последний вопрос!');
        $(".question[data-question-number=" + prevQuestionNumber + "]").toggle('action');
        $(".question[data-question-number=" + (prevQuestionNumber + 1) + "]").toggle('action');
        setTimeout(function(){
          clearInterval(refreshIntervalId);
          $(".content").html('<span style="color: green"><b>Тестирование завершено!</b></span>');
        }, 10000);
    }
  }

  $("body").on("click", "#next", function() {
    var prevQuestionNumber = $(".question:visible").data("question-id");
    clearTimeout(refreshTimeoutId);
    nextQuestion(prevQuestionNumber);
  });
</script>
