<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use backend\models\Question;
use backend\models\QuestionForm;
use backend\models\QuestionList;
use backend\models\QuestionListForm;
use backend\models\Candidate;
use backend\models\CandidateForm;

class OperationController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [
                            'addquestion', 
                            'deletequestion', 
                            'updatequestion', 
                            'addquestionlist', 
                            'updatequestionlist', 
                            'deletequestionlist',
                            'addcandidate',
                            'updatecandidate',
                            'deletecandidate',
                            'getquestionstolist',
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    /*[
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],*/
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'logout' => ['updatequestion'],
                ],
            ],
        ];
    }

    public function actionGetquestionstolist()
    {  
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        if (isset(Yii::$app->request->get()['question_list_id'])) {
            $listId = Yii::$app->request->get()['question_list_id'][0];
            $questionsToList = Question::find()->where(['list_id' => $listId])->all();
            if (count($questionsToList)) {
                return \yii\helpers\Json::encode($questionsToList);
            } 
        }

        return false;
    }

    public function actionAddquestion()
    {   
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        
        $model = new QuestionForm(); 
        if ($model->load(Yii::$app->request->post(),'')) {
            if ($model->addQuestion()) {
                $listId = Yii::$app->request->post('list_id');
                return $this->redirect('/backend/web/site/index?list_id=' . $listId, 302);
                //return $this->goHome();
            }
        } 

        if (Yii::$app->request->isAjax) {
            $listId = Yii::$app->request->get('list_id');
            return $this->renderAjax('question', [
                'model' => $model,
                'listId' => $listId
            ]);
        }

        $this->layout = '@backend/views/layouts/main';
        return $this->render('question', [
            'model' => $model,
        ]);
    }

    public function actionUpdatequestion()
    {  
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new QuestionForm();
        if ($model->load(Yii::$app->request->post(),'')) {
           if ($model->updateQuestion()) {
                return $this->goHome();
            }
        } 

        $questionIds = preg_replace('/^:|:$/', '', Yii::$app->request->post('question_ids'));
        $questionIds = explode('::', $questionIds)[0];
        $model = Question::findById($questionIds);
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('update_question', [
                'model' => $model,
            ]);
        }

        $this->layout = '@backend/views/layouts/main';
        return $this->render('update_question', [
            'model' => $model,
        ]);
    }

    public function actionDeletequestion()
    {   
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        if (Yii::$app->request->isPost && Yii::$app->request->isAjax) {
            if (Yii::$app->request->post('question_ids')) {
                $questionIds = Yii::$app->request->post('question_ids');
                $questionIds = preg_replace('/^:|:$/', '', $questionIds);
                $questionIds = explode('::', $questionIds);
                if (count($questionIds) > 1) {
                    foreach ($questionIds as $questionId) {
                        Question::findOne($questionId)->delete();
                    }
                } else if (count($questionIds) == 1) {
                    Question::findOne($questionIds[0])->delete();
                }  
                return true;           
            }
        }

        return $this->goHome();
    }

    public function actionAddquestionlist()
    {   
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new QuestionListForm(); 
        if ($model->load(Yii::$app->request->post(),'')) {
            if ($model->addQuestionList()) {
                return $this->goHome();
            }
        } 

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('question_list', [
                'model' => $model
            ]);
        }

        $this->layout = '@backend/views/layouts/main';
        return $this->render('question_list', [
            'model' => $model,
        ]);
    }

    public function actionUpdatequestionlist()
    {  
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new QuestionListForm();
        if ($model->load(Yii::$app->request->post(),'')) {
           if ($model->updateQuestionList()) {
                return $this->goHome();
            }
        } 

        $questionListIds = preg_replace('/^:|:$/', '', Yii::$app->request->post('question_list_ids'));
        $questionListIds = explode('::', $questionListIds)[0];
        $model = QuestionList::findById($questionListIds);
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('update_question_list', [
                'model' => $model,
            ]);
        }

        $this->layout = '@backend/views/layouts/main';
        return $this->render('update_question_list', [
            'model' => $model,
        ]);
    }

    public function actionDeletequestionlist()
    {   
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        if (Yii::$app->request->isPost && Yii::$app->request->isAjax) {
            if (Yii::$app->request->post('question_list_ids')) {
                $questionListIds = Yii::$app->request->post('question_list_ids');
                $questionListIds = preg_replace('/^:|:$/', '', $questionListIds);
                $questionListIds = explode('::', $questionListIds);
                if (count($questionListIds) > 1) {
                    foreach ($questionListIds as $questionListId) {
                        $questionsToList = Question::find()->where(['list_id' => $questionListId])->all();
                        foreach ($questionsToList as $questionToList) {
                            $questionToList->list_id = null;
                            $questionToList->save();
                        }
                        if (!QuestionList::findOne($questionListId)->delete()) {
                            foreach ($questionsToList as $questionToList) {
                                $questionToList->list_id = $questionListId;
                                $questionToList->save();
                            }
                        }
                    }
                } else if (count($questionListIds) == 1) {
                    $questionsToList = Question::find()->where(['list_id' => $questionListIds[0]])->all();
                    foreach ($questionsToList as $questionToList) {
                            $questionToList->list_id = null;
                            $questionToList->save();
                    }
                    if (!QuestionList::findOne($questionListIds[0])->delete()) {
                        foreach ($questionsToList as $questionToList) {
                            $questionToList->list_id = $questionListIds[0];
                            $questionToList->save();
                        }
                    }
                }  
                return true;           
            }
        }

        return $this->goHome();
    }

    public function actionAddcandidate()
    {   
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new CandidateForm();
        if ($model->load(Yii::$app->request->post(),'')) {
            if ($model->addCandidate()) {
                return $this->redirect('/backend/web/site/index?candidates=true');
            }
        } 

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('candidate', [
                'model' => $model
            ]);
        }

        $this->layout = '@backend/views/layouts/main';
        return $this->render('candidate', [
            'model' => $model
        ]);
    }

    public function actionUpdatecandidate()
    {  
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new CandidateForm();

        if ($model->load(Yii::$app->request->post(),'')) {
           if ($model->updateCandidate()) {
                return $this->goHome();
            }
        } 

        $candidateIds = preg_replace('/^:|:$/', '', Yii::$app->request->post('candidate_ids'));
        $candidateIds = explode('::', $candidateIds)[0];
        $model = Candidate::findById($candidateIds);
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('update_candidate', [
                'model' => $model,
            ]);
        }

        $this->layout = '@backend/views/layouts/main';
        return $this->render('update_candidate', [
            'model' => $model,
        ]);
    }

    public function actionDeletecandidate()
    {   
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        if (Yii::$app->request->isPost && Yii::$app->request->isAjax) {
            if (Yii::$app->request->post('candidate_ids')) {
                $candidateIds = Yii::$app->request->post('candidate_ids');
                $candidateIds = preg_replace('/^:|:$/', '', $candidateIds);
                $candidateIds = explode('::', $candidateIds);
                if (count($candidateIds) > 1) {
                    foreach ($candidateIds as $candidateId) {
                        Candidate::findOne($candidateId)->delete();
                    }
                } else if (count($candidateIds) == 1) {
                    Candidate::findOne($candidateIds[0])->delete();
                }  
                return true;           
            }
        }

        return $this->goHome();
    }
}
