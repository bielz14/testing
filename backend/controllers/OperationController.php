<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use backend\models\Question;
use backend\models\QuestionForm;
use backend\models\QuestionList;
use backend\models\QuestionListForm;
use backend\models\Candidate;
use backend\models\CandidateForm;
use yii\helpers\ArrayHelper;

class OperationController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [
                            'addquestion', 
                            'deletequestion', 
                            'updatequestion', 
                            'addquestionlist', 
                            'updatequestionlist', 
                            'deletequestionlist',
                            'addcandidate',
                            'updatecandidate',
                            'deletecandidate',
                            'getquestionstolist',
                            'invitecandidate',
                            'uploadvideo',
                            'setcandidatetested'
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => [
                            'uploadvideo',
                            'setcandidatetested'
                        ],
                        'allow' => true,
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'logout' => ['updatequestion'],
                ],
            ],
        ];
    }

    public function actionGetquestionstolist()
    {  
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        if (isset(Yii::$app->request->get()['question_list_id'])) {
            $listId = Yii::$app->request->get()['question_list_id'][0];
            $questionList = QuestionList::findOne($listId);
            if ($questionList->author_id == Yii::$app->user->identity->id) {
                $questionsToList = Question::find()->where(['list_id' => $listId])->all();
                if (count($questionsToList)) {
                    return \yii\helpers\Json::encode($questionsToList);
                }
            }      
        }

        return false;
    }

    public function actionAddquestion()
    {   
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        
        $model = new QuestionForm(); 
        if ($model->load(Yii::$app->request->post(),'')) {
            if ($model->addQuestion()) {
                $listId = Yii::$app->request->post('list_id');
                return $this->redirect('/backend/web/site/index?list_id=' . $listId, 302);
            }  
        } 

        if (Yii::$app->request->isAjax) {
            $listId = Yii::$app->request->get('list_id');
            return $this->renderAjax('question', [
                'model' => $model,
                'listId' => $listId
            ]);
        }

        $this->layout = '@backend/views/layouts/main';
        return $this->render('question', [
            'model' => $model,
        ]);
    }

    public function actionUpdatequestion()
    {  
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new QuestionForm();
        if ($model->load(Yii::$app->request->post(),'')) {
            if ($question = $model->updateQuestion()) {
                return $this->redirect('/backend/web/site/index?list_id=' . $question->list_id);
            }
        } 

        $questionIds = [Yii::$app->request->post('id')];
        if (!$questionIds[0]) {
            $questionIds = preg_replace('/^:|:$/', '', Yii::$app->request->post('question_ids'));
            $questionIds = explode('::', $questionIds);
        }

        $model = Question::findById($questionIds[0]);
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('update_question', [
                'model' => $model,
            ]);
        }

        $this->layout = '@backend/views/layouts/main';
        return $this->render('update_question', [
            'model' => $model,
        ]);
    }

    public function actionDeletequestion()
    {   
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        if (Yii::$app->request->isPost && Yii::$app->request->isAjax) {
            if (Yii::$app->request->post('question_ids')) {
                $questionIds = Yii::$app->request->post('question_ids');
                $questionIds = preg_replace('/^:|:$/', '', $questionIds);
                $questionIds = explode('::', $questionIds);
                if (count($questionIds) > 1) {
                    foreach ($questionIds as $questionId) {
                        $question = Question::findOne($questionId);
                        if ($question->questionList->author_id == Yii::$app->user->identity->id) {
                            Question::findOne($questionId)->delete();
                        } else {
                            return false;
                        }
                    }
                } else if (count($questionIds) == 1) {
                    $question = Question::findOne($questionIds[0]);
                    if ($question->questionList->author_id == Yii::$app->user->identity->id) {
                        Question::findOne($questionIds[0])->delete();
                    } else {
                        return false;
                    }
                }  
                return true;           
            }
        }

        return $this->goHome();
    }

    public function actionAddquestionlist()
    {   
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new QuestionListForm(); 
        if ($model->load(Yii::$app->request->post(),'')) {
            if ($model->addQuestionList()) {
                return $this->goHome();
            }
        } 

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('question_list', [
                'model' => $model
            ]);
        }

        $this->layout = '@backend/views/layouts/main';
        return $this->render('question_list', [
            'model' => $model,
        ]);
    }

    public function actionUpdatequestionlist()
    {  
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new QuestionListForm();
        if ($model->load(Yii::$app->request->post(),'')) {
            if ($questionList = $model->updateQuestionList()) {
                return $this->redirect('/backend/web/site/index?list_id=' . $questionList->id);
            }
        } 

        $questionListIds = [Yii::$app->request->post('id')];
        if (!$questionListIds[0]) {
            $questionListIds = preg_replace('/^:|:$/', '', Yii::$app->request->post('question_list_ids'));
            $questionListIds = explode('::', $questionListIds);
        }

        $model = QuestionList::findById($questionListIds[0]);
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('update_question_list', [
                'model' => $model,
            ]);
        }

        $this->layout = '@backend/views/layouts/main';
        return $this->render('update_question_list', [
            'model' => $model,
        ]);
    }

    public function actionDeletequestionlist()
    {   
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        if (Yii::$app->request->isPost && Yii::$app->request->isAjax) {
            if (Yii::$app->request->post('question_list_ids')) {
                $questionListIds = Yii::$app->request->post('question_list_ids');
                $questionListIds = preg_replace('/^:|:$/', '', $questionListIds);
                $questionListIds = explode('::', $questionListIds);
                if (count($questionListIds) > 1) {
                    foreach ($questionListIds as $questionListId) {
                        $questionList = QuestionList::findOne($questionListId);
                        if ($questionList->author_id == Yii::$app->user->identity->id) {
                            if ($questionList->delete()) {
                                $questionsToList = Question::find()->where(['list_id' => $questionListId])->all();
                                foreach ($questionsToList as $questionToList) {
                                    $questionToList->delete();
                                }
                            }
                        } else {
                            return false;
                        }
                    }
                } else if (count($questionListIds) == 1) {
                    $questionList = QuestionList::findOne($questionListIds[0]);
                    if ($questionList->author_id == Yii::$app->user->identity->id) {
                        $questionsToList = Question::find()->where(['list_id' => $questionListIds[0]])->all();
                        if (QuestionList::findOne($questionListIds[0])->delete()) {
                            foreach ($questionsToList as $questionToList) {
                                $questionToList->delete();
                            }
                        }
                    } else {
                         return false;
                    }
                }  
                return true;           
            }
        }

        return $this->goHome();
    }

    public function actionAddcandidate()
    {   
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new CandidateForm();
        if ($model->load(Yii::$app->request->post(),'')) {
            if ($model->addCandidate()) {
                return $this->redirect('/backend/web/site/index?candidates=true');
            }
        } 
        $questionLists = QuestionList::find()->where(['author_id' => Yii::$app->user->identity->id])->all();
        $questionListsDataToForm = [];
        foreach ($questionLists as $questionList) {
            $questionListsDataToForm[$questionList->id] = $questionList->title;
        }

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('candidate', [
                'model' => $model,
                'questionLists' => $questionListsDataToForm
            ]);
        }

        $this->layout = '@backend/views/layouts/main';
        return $this->render('candidate', [
            'model' => $model,
            'questionLists' => $questionListsDataToForm
        ]);
    }

    public function actionUpdatecandidate()
    {  
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new CandidateForm();

        if ($model->load(Yii::$app->request->post(),'')) {
            if ($model->updateCandidate()) {
                return $this->redirect('/backend/web/site/index?candidates=true');
            }
        } 

        $candidateIds = [Yii::$app->request->post('id')];
        if (!$candidateIds[0]) {
            $candidateIds = preg_replace('/^:|:$/', '', Yii::$app->request->post('candidate_ids'));
            $candidateIds = explode('::', $candidateIds);
        }

        $questionLists = QuestionList::find()->where(['author_id' => Yii::$app->user->identity->id])->all();
        $questionListsDataToForm = [];
        foreach ($questionLists as $questionList) {
            $questionListsDataToForm[$questionList->id] = $questionList->title;
        }

        $model = Candidate::findById($candidateIds[0]);
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('update_candidate', [
                'model' => $model,
                'questionLists' => $questionListsDataToForm
            ]);
        }

        $this->layout = '@backend/views/layouts/main';
        return $this->render('update_candidate', [
            'model' => $model,
            'questionLists' => $questionListsDataToForm
        ]);
    }

    public function actionDeletecandidate()
    {   
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        if (Yii::$app->request->isPost && Yii::$app->request->isAjax) {
            if (Yii::$app->request->post('candidate_ids')) {
                $candidateIds = Yii::$app->request->post('candidate_ids');
                $candidateIds = preg_replace('/^:|:$/', '', $candidateIds);
                $candidateIds = explode('::', $candidateIds);
                if (count($candidateIds) > 1) {
                    foreach ($candidateIds as $candidateId) {
                        $candidate = Candidate::findOne($candidateId);
                        if ($candidate->author_id == Yii::$app->user->identity->id) {
                            $candidate->delete();
                        }  
                    }
                } else if (count($candidateIds) == 1) {
                    $candidate = Candidate::findOne($candidateIds[0]);
                    if ($candidate->author_id == Yii::$app->user->identity->id) {
                        $candidate->delete();
                    } 
                }  
                return true;           
            }
        }

        return $this->goHome();
    }

    public function actionInvitecandidate()
    {
        $candidateIds = Yii::$app->request->post('candidate_ids');
        $candidateIds = preg_replace('/^:|:$/', '', $candidateIds);
        $candidateIds = explode('::', $candidateIds);
        $invitingCandidate = [];
        foreach ($candidateIds as $candidateId) {
            $candidate = Candidate::findOne($candidateId); 
            if ($candidate && !$candidate->invited) {
                $questionsToList = Question::find()->where(['list_id' => $candidate->list_id])->all();
                if (count($questionsToList)) {
                    $result = Yii::$app->mailer->compose(['html' => 'candidateInvite-html'], ['candidate_id' => $candidateId])
                        ->setFrom(Yii::$app->user->identity->email)
                        ->setTo($candidate->email)
                        ->setSubject('lime-prime')
                        ->send();

                    if ($result) {
                        $candidate->invited = true;
                        date_default_timezone_set('Europe/Kiev');
                        $candidate->invited_date = date("d-m-Y H:i:s");
                        $candidate->save();
                        $invitingCandidates[$candidateId] = $candidate->name;
                    }
                }
            }
        }

        if (count($invitingCandidates)) {
            return json_encode($invitingCandidates);
        }

        return false;
    }

    /*public function actionInvitecandidate()
    {
        $candidateIds = Yii::$app->request->post('candidate_ids');
        $candidateIds = preg_replace('/^:|:$/', '', $candidateIds);
        $candidateIds = explode('::', $candidateIds);
        $candidate = Candidate::findOne($candidateIds[0]); 
        if ($candidate && !$candidate->invited) {
            $questionsToList = Question::find()->where(['list_id' => $candidate->list_id])->all();
            if (count($questionsToList)) {
                $result = Yii::$app->mailer->compose(['html' => 'candidateInvite-html'], ['candidate_id' => $candidateIds[0]])
                    ->setFrom(Yii::$app->user->identity->email)
                    ->setTo($candidate->email)
                    ->setSubject('lime-prime')
                    ->send();

                if ($result) {
                    $candidate->invited = true;
                    date_default_timezone_set('Europe/Kiev');
                    $candidate->invited_date = date("d-m-Y H:i:s");
                    $candidate->save();
                    return 1;
                } else {
                    return 2;
                }
            } else {
                return 3;
            }

        } else {
          return 0;  
        }
    }*/

    public function actionUploadvideo()
    {
        if (isset($_FILES['video'])) {
            $fileName = 'video_of_testing_candidate' . Yii::$app->request->post('candidate_id') . '.webm';
            $uploadDirectory = Yii::getAlias('@app') . '/web/uploads/' . $fileName;
            if (!move_uploaded_file($_FILES['video']['tmp_name'], $uploadDirectory)) {
                echo('Couldn\'t upload video !');
            } else {
                $candidate = Candidate::findOne(Yii::$app->request->post('candidate_id'));
                $candidate->video_url = Yii::$app->getUrlManager()->getBaseUrl() . '/uploads' . '/' . $fileName;
                $candidate->save();
            }
        } else {
            echo 'No file uploaded';
        }
    }

    public function actionSetcandidatetested()
    {
        $candidate = Candidate::findOne(Yii::$app->request->post('candidate_id'));
        //$candidate->tested = true;
        if (!$candidate->save()) {
            return false;
        }
        return true;
    }
}
