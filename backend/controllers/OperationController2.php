<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use backend\models\QuestionListForm;
use backend\models\QuestionForm;
use backend\models\Question;
use backend\models\QuestionList;

class OperationController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [
                            'addquestion', 
                            'deletequestion', 
                            'updatequestion', 
                            'addquestionlist', 
                            'updatequestionlist', 
                            'deletequestionlist'
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    /*[
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],*/
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'logout' => ['updatequestion'],
                ],
            ],
        ];
    }

    public function actionAddquestion()
    {   
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new QuestionForm(); 
        if ($model->load(Yii::$app->request->post(),'')) {
            if ($model->addQuestion()) {
                return $this->goHome();
            }
        } 

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('question', [
                'model' => $model,
            ]);
        }

        $this->layout = '@backend/views/layouts/main';
        return $this->render('question', [
            'model' => $model,
        ]);
    }

    public function actionUpdatequestion()
    {  
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new QuestionForm();
        if ($model->load(Yii::$app->request->post(),'')) {
           if ($model->updateQuestion()) {
                return $this->goHome();
            }
        } 

        $questionIds = preg_replace('/^:|:$/', '', Yii::$app->request->post('question_ids'));
        $questionIds = explode('::', $questionIds)[0];
        $model = Question::findById($questionIds);
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('update_question', [
                'model' => $model,
            ]);
        }

        $this->layout = '@backend/views/layouts/main';
        return $this->render('update_question', [
            'model' => $model,
        ]);
    }

    public function actionDeletequestion()
    {   
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        if (Yii::$app->request->isPost && Yii::$app->request->isAjax) {
            if (Yii::$app->request->post('question_ids')) {
                $questionIds = Yii::$app->request->post('question_ids');
                $questionIds = preg_replace('/^:|:$/', '', $questionIds);
                $questionIds = explode('::', $questionIds);
                if (count($questionIds) > 1) {
                    foreach ($questionIds as $questionId) {
                        Question::findOne($questionId)->delete();
                    }
                } else if (count($questionIds) == 1) {
                    Question::findOne($questionIds[0])->delete();
                }  
                return true;           
            }
        }

        return $this->goHome();
    }

    public function actionAddquestionlist()
    {   
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new QuestionListForm(); 
        if ($model->load(Yii::$app->request->post(),'')) {
            if ($questionList = $model->addQuestionList()) {
                $questionIds = Yii::$app->request->post('question_ids');
                $questionIds = preg_replace('/^:|:$/', '', $questionIds);
                $questionIds = explode('::', $questionIds);
                $questions = Question::find()->where(['in', 'id', $questionIds])->all();
                foreach ($questions as $question) {
                    $question->list_id = $questionList->id;
                    $question->save();
                }
                $questionToList = Question::find()->where(['in', 'list_id', $questionList->id])->one();
                if (!count($questionToList)) {
                    $questionList->delete();
                }
                return $this->goHome();
            }
        } 

        if (Yii::$app->request->isAjax) {
            $questionIds = Yii::$app->request->get('question_ids');
            return $this->renderAjax('question_list', [
                'model' => $model,
                'questionIds' => $questionIds
            ]);
        }

        $this->layout = '@backend/views/layouts/main';
        return $this->render('question_list', [
            'model' => $model,
        ]);
    }

    public function actionUpdatequestionlist()
    {  
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new QuestionListForm();
        if ($model->load(Yii::$app->request->post(),'')) {
           if ($model->updateQuestionList()) {
                return $this->goHome();
            }
        } 

        $questionListIds = preg_replace('/^:|:$/', '', Yii::$app->request->post('question_list_ids'));
        $questionListIds = explode('::', $questionListIds)[0];
        $model = QuestionList::findById($questionListIds);
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('update_question_list', [
                'model' => $model,
            ]);
        }

        $this->layout = '@backend/views/layouts/main';
        return $this->render('update_question_list', [
            'model' => $model,
        ]);
    }

    public function actionDeletequestionlist()
    {   
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        if (Yii::$app->request->isPost && Yii::$app->request->isAjax) {
            if (Yii::$app->request->post('question_list_ids')) {
                $questionListIds = Yii::$app->request->post('question_list_ids');
                $questionListIds = preg_replace('/^:|:$/', '', $questionListIds);
                $questionListIds = explode('::', $questionListIds);
                if (count($questionListIds) > 1) {
                    foreach ($questionListIds as $questionListId) {
                        $questionsToList = Question::find()->where(['list_id' => $questionListId])->all();
                        foreach ($questionsToList as $questionToList) {
                            $questionToList->list_id = null;
                            $questionToList->save();
                        }
                        if (!QuestionList::findOne($questionListId)->delete()) {
                            foreach ($questionsToList as $questionToList) {
                                $questionToList->list_id = $questionListId;
                                $questionToList->save();
                            }
                        }
                    }
                } else if (count($questionListIds) == 1) {
                    $questionsToList = Question::find()->where(['list_id' => $questionListIds[0]])->all();
                    foreach ($questionsToList as $questionToList) {
                            $questionToList->list_id = null;
                            $questionToList->save();
                    }
                    if (!QuestionList::findOne($questionListIds[0])->delete()) {
                        foreach ($questionsToList as $questionToList) {
                            $questionToList->list_id = $questionListIds[0];
                            $questionToList->save();
                        }
                    }
                }  
                return true;           
            }
        }

        return $this->goHome();
    }
}
