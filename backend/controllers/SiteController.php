<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use backend\models\Question;
use backend\models\QuestionList;
use backend\models\Candidate;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error', 'testing'],
                        'allow' => true
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $userId = Yii::$app->user->identity->id;
        if (Yii::$app->request->get('list_id')) {
            $currentQuestionList = QuestionList::findById(Yii::$app->request->get('list_id'));
            if ($currentQuestionList->author_id == $userId) {
                $questions = Question::find()->where(['list_id' => Yii::$app->request->get('list_id')])->orderBy(['position' => SORT_ASC])->all();
            }
        } else {
            $questionList = QuestionList::find()->where(['author_id' => $userId])
                                          ->orderBy(['id' => SORT_ASC])
                                          ->limit(1)->all();
            if (isset($questionList[0])) {
              $listId = $questionList[0]->id;
              $questions = Question::find()->where(['list_id' => $listId])->orderBy(['position' => SORT_ASC])->all();
            }
            
        }
     
        $questionLists = QuestionList::find()->where(['author_id' => $userId])->all();
        $candidates = Candidate::find()
                        ->where(['candidate.author_id' => $userId])
                        ->leftJoin('question_list', '`question_list`.`id` = `candidate`.`list_id`')
                        ->all();     

        return $this->render('index', [
             'questions' => $questions,
             'questionLists' => $questionLists,
             'candidates' => $candidates,
             'questionTab' => true,
        ]);
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm(); 
        if ($model->load(Yii::$app->request->post(),'') && $model->login()) {     
            return $this->goBack(); 
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionTesting()
    {
        $candidateId = Yii::$app->request->get('candidate_id');
        $candidate = Candidate::findOne($candidateId);
        if ($candidate->tested) {
          $testing = true;
        } else if (!$candidate) {
          $testing = false;
        } else {
          $listId = $candidate->list_id;
          $questions = Question::find()->where(['list_id' => $listId])->orderBy(['position' => SORT_ASC])->all();

          //$this->layout = '@backend/views/layouts/main';
          return $this->render('/site/testing', [
              'questions' => $questions
          ]);
        }

        return $this->render('/site/testing', [
            'testing' => $testing
        ]);
    }
}
