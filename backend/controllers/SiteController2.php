<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use backend\models\Question;
use backend\models\QuestionList;
use backend\models\Candidate;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        if (Yii::$app->request->get('list_id')) {
            $questions = Question::find()->where(['list_id' => Yii::$app->request->get('list_id')])->all();
        } else {
            $listId = QuestionList::find()->orderBy(['id' => SORT_ASC])->limit(1)->all()[0]->id;
            $questions = Question::find()->where(['list_id' => $listId])->all();
        }

        $questionLists = QuestionList::find()->all();
        $candidates = Candidate::find()
                        ->leftJoin('question_list', '`question_list`.`id` = `candidate`.`list_id`')
                        ->all();     

        return $this->render('index', [
             'questions' => $questions,
             'questionLists' => $questionLists,
             'candidates' => $candidates,
             'questionTab' => true,
        ]);
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm(); 
        if ($model->load(Yii::$app->request->post(),'') && $model->login()) {     
            return $this->goBack(); 
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
