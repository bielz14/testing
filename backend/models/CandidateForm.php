<?php
namespace backend\models;

use Yii;
use yii\base\Model;
use yii\behaviors\TimestampBehavior;

/**
 * Login form
 */
class CandidateForm extends Model
{
    public $id;
    public $name;
    public $email;
    public $list_id;
    //public $created_at;
    //public $updated_at;
    public $video_url;

    private $_question;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['id', 'integer'],
            ['name', 'required'],
            ['name', 'string'],
            ['email', 'required'],
            ['email', 'email'],
            ['list_id', 'integer'],
            //[['created_at', 'updated_at'], 'safe'],
            //['created_at', 'string'],
            //['video_url', 'string'],
        ];
    }

    /*public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    \yii\db\BaseActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    \yii\db\BaseActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => date('Y-m-d H:i:s'),
            ],
        ];
    }*/

    protected function getCandidate()
    {
        if ($this->_candidate === null) {
            $this->_candidate = Candidate::findById($this->id);
        }

        return $this->_candidate;
    }

    public function addCandidate()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $candidate = new Candidate();
        $candidate->name = $this->name;
        $candidate->email = $this->email;
        $candidate->list_id = $this->list_id;
        date_default_timezone_set('Europe/Kiev');
        $candidate->created_at = date("d-m-Y H:i:s");
        $candidate->author_id = Yii::$app->user->identity->id;
        
        return $candidate->save() ? $candidate : null;
    }

    public function updateCandidate()
    {   
        if (!$this->validate()) {
            return null;
        }

        $candidate = Candidate::findById($this->id);
        $candidate->name = $this->name;
        $candidate->email = $this->email;
        $candidate->list_id = $this->list_id;

        return $candidate->save() ? $candidate : null;
    }
}
