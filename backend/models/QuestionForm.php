<?php
namespace backend\models;

use Yii;
use yii\base\Model;

/**
 * Login form
 */
class QuestionForm extends Model
{
    public $id;
    public $title;
    public $list_id;
    public $position;
    public $time;

    private $_question;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['id', 'integer'],
            ['title', 'required'],
            ['title', 'string'],
            ['list_id', 'integer'],
            ['position', 'integer'],
            ['position', 'required'],
            ['time', 'integer'],
            ['time', 'required'],
        ];
    }

    protected function getQuestion()
    {
        if ($this->_question === null) {
            $this->_question = Question::findById($this->id);
        }

        return $this->_question;
    }

    public function addQuestion()
    {        
        $question = new Question();
        $questionList = QuestionList::findOne($this->list_id);
        if ($this->validate() && $questionList->author_id == Yii::$app->user->identity->id) {
            $question->title = $this->title;
            $question->position = $this->position;
            $question->time = $this->time;
            if ($this->list_id) {
                $question->list_id = $this->list_id;
            }
            if ($question->save()) {
                return $question;
            }
        }

        return null;
    }

    public function updateQuestion()
    {   
        $question = Question::findById($this->id);
        if ($this->validate() && $question->questionList->author_id == Yii::$app->user->identity->id) {
            $question->title = $this->title;
            $question->position = $this->position;
            $question->time = $this->time;
            if ($question->save()) {
                return $question;
            }
        }
        
        return null;
    }
}
