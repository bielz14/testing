<?php
namespace backend\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * QuestionList model
 *
 * @property integer $id
 * @property string $name
 * @property integer $list_id
 * @property string $created_at
 * @property string $updated_at
 * @property string $video_url
 */
class Candidate extends ActiveRecord implements IdentityInterface
{
    public static function tableName()
    {
        return '{{%candidate}}';
    }

    public function getId()
    {
        return $this->getPrimaryKey();
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    public static function findById($id)
    {
        return static::findOne(['id' => $id]);
    }

    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    public function getQuestionList()
    {
        return $this->hasOne(QuestionList::className(), ['id' => 'list_id']);
    }
}
