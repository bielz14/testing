<?php
namespace backend\models;

use Yii;
use yii\base\Model;

/**
 * Login form
 */
class QuestionListForm extends Model
{
    public $id;
    public $title;

    private $_questionList;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['id', 'integer'],
            ['title', 'required'],
            ['title', 'string'],
        ];
    }

    protected function getQuestionList()
    {
        if ($this->_questionList === null) {
            $this->_questionList = QuestionList::findById($this->id);
        }

        return $this->_questionList;
    }

    public function addQuestionList()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $question = new QuestionList();
        $question->title = $this->title;
        $question->author_id = Yii::$app->user->identity->id;
        
        return $question->save() ? $question : null;
    }

    public function updateQuestionList()
    {   
        $questionList = QuestionList::findById($this->id);
        if ($this->validate() && $questionList->author_id == Yii::$app->user->identity->id) {
            $questionList->title = $this->title;
            if ($questionList->save()) {
                return $questionList;
            }
        }
        
        return null;
    }
}
