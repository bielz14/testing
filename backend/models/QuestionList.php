<?php
namespace backend\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use common\models\User;

/**
 * QuestionList model
 *
 * @property integer $id
 * @property string $title
 */
class QuestionList extends ActiveRecord implements IdentityInterface
{
    public static function tableName()
    {
        return '{{%question_list}}';
    }

    public function getId()
    {
        return $this->getPrimaryKey();
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }
    
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    public static function findById($id)
    {
        return static::findOne(['id' => $id]);
    }

    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    public function getQuestions()
    {
        return $this->hasMany(Question::className(), ['list_id' => 'id']);
    }

    public function getCandidates()
    {
        return $this->hasMany(Candidate::className(), ['list_id' => 'id']);
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }
}
